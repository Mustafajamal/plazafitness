<?php 
$obj_membership_payment=new Gmgt_membership_payment;
$active_tab=isset($_REQUEST['tab'])?$_REQUEST['tab']:'paymentlist';

	if(isset($_POST['add_fee_payment']))
	{
		//POP up data save in payment history
		if($_POST['payment_method'] == 'Paypal')
		{				
		require_once GMS_PLUGIN_DIR. '/lib/paypal/paypal_process.php';				
		}
		else
		{			
		$result=$obj_membership_payment->add_feespayment_history($_POST);		
			if($result)
			{
				wp_redirect ( home_url() . '?dashboard=user&page=membership_payment&message=1');
			}
		}
	}
	if(isset($_REQUEST['action'])&& $_REQUEST['action']=='success')
	{
	?>
		<div id="message" class="updated below-h2 ">
			<p>
			<?php 
				_e('Payment successfully','school-mgt');
			?></p></div>
	<?php
	}
	if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'ipn')
	{$trasaction_id  = $_POST["txn_id"];
$custom_array = explode("_",$_POST['custom']);
$feedata['mp_id']=$custom_array[1];

$feedata['amount']=$_POST['mc_gross_1'];
$feedata['payment_method']='paypal';	
$feedata['trasaction_id']=$trasaction_id ;
//$log_array		= print_r($feedata, TRUE);
	//	wp_mail( 'maks.ashvin03@gmail.com', 'Schoolpaypal', $log_array);

$obj_membership_payment->add_feespayment_history($feedata);
		//require_once SMS_PLUGIN_DIR. '/lib/paypal/paypal_ipn.php';
	}
	if(isset($_REQUEST['action'])&& $_REQUEST['action']=='cancel')
	{?>
		<div id="message" class="updated below-h2 ">
			<p>
			<?php 
				_e('Payment Cancel','school-mgt');
			?></p></div>
			<?php
	}
	
if(isset($_REQUEST['message']))
{
	$message =$_REQUEST['message'];
	if($message == 1)
	{?>
			<div id="message" class="updated below-h2 ">
			<p>
			<?php 
				_e('Record inserted successfully','school-mgt');
			?></p></div>
			<?php 
		
	}
	elseif($message == 2)
	{?><div id="message" class="updated below-h2 "><p><?php
				_e("Record updated successfully.",'school-mgt');
				?></p>
				</div>
			<?php 
		
	}
	elseif($message == 3) 
	{?>
	<div id="message" class="updated below-h2"><p>
	<?php 
		_e('Record deleted successfully','school-mgt');
	?></div></p><?php
			
	}
}	
	?>


<!-- POP up code -->
<div class="popup-bg">
    <div class="overlay-content">
    <div class="modal-content">
    <div class="invoice_data">
     </div>
    </div>
    </div> 
</div>
<!-- End POP-UP Code -->
<script>
$(document).ready(function() {
    $('#payment_list').DataTable({
        "responsive": true
    });
} );
</script>
<?php //$retrieve_class =  get_payment_list();?>
<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
      <li class="<?php if($active_tab=='paymentlist'){?>active<?php }?>">
          <a href="?dashboard=user&page=feepayment&tab=paymentlist"  class="tab <?php echo $active_tab == 'paymentlist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php _e('Membership Payment', 'gym_mgt'); ?></a>
          </a>
      </li>
      
	  
	
	
</ul>
		<div class="tab-content">
		
		<div class="panel-body">
        <table id="payment_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php  _e( 'Title', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Amount', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Due Amount', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Membership <BR>Start Date', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Membership <BR>End Date', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Payment Status', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </thead>
		<tfoot>
            <tr>
			<th><?php  _e( 'Title', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Amount', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Due Amount', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Membership <BR>Start Date', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Membership <BR>End Date', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Payment Status', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </tfoot>
		<tbody>
         <?php 
		
			if($obj_gym->role == 'member'){
			$paymentdata=get_all_membership_payment_byuserid($user_id);
			}
			else
			{
				$paymentdata=$obj_membership_payment->get_all_membership_payment();
			}
		 if(!empty($paymentdata))
		 {
		 	foreach ($paymentdata as $retrieved_data){

		 ?>
            <tr>
				<td class="productname"><?php echo get_membership_name($retrieved_data->membership_id);?></td>
				<td class="paymentby"><?php $user=get_userdata($retrieved_data->member_id);
					$memberid=get_user_meta($retrieved_data->member_id,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;
					?></td>
				<td class="totalamount"><?php echo $retrieved_data->membership_amount;?></td>
				<td class="totalamount"><?php echo $retrieved_data->membership_amount-$retrieved_data->paid_amount;?></td>
				<td class="paymentdate"><?php echo $retrieved_data->start_date;?></td>
				<td class="paymentdate"><?php echo $retrieved_data->end_date;?></td>
				<td class="paymentdate">
				<?php 
				echo "<span class='btn btn-success btn-xs'>";
				echo get_membership_paymentstatus($retrieved_data->mp_id);
				echo "</span>";
				?>
				</td>
                
               	<td class="action">
				<a href="#" class="show-payment-popup btn btn-default" idtest="<?php echo $retrieved_data->mp_id; ?>" view_type="payment" ><?php _e('Pay','gym_mgt');?></a>
				<a href="#" class="show-view-payment-popup btn btn-default" idtest="<?php echo $retrieved_data->mp_id; ?>" view_type="view_payment"><?php _e('View','gym_mgt');?></a>                
                </td>
               
            </tr>
            <?php } 
			
		}?>
     
        </tbody>
        
        </table>
        </div>
        <?php 
       
        
        ?>
        </div>
        </div>
 <?php ?>