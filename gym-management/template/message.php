<?php 
$obj_gym = new Gym_management(get_current_user_id());

$active_tab = isset($_GET['tab'])?$_GET['tab']:'inbox';
	?>
<div id="main-wrapper">
<div class="row mailbox-header">
                                <div class="col-md-2">
                                <?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant'){?>
                                    <a class="btn btn-success btn-block" href="?dashboard=user&page=message&tab=compose">
                                    <?php _e("Compose","gym_mgt");?></a>
                                   <?php }?>
                                </div>
                                <div class="col-md-6">
                                    <h2>
                                    <?php
									if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox'))
                                    echo esc_html( __( 'Inbox', 'gym_mgt' ) );
									else if(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'sentbox')
									echo esc_html( __( 'Sent Item', 'gym_mgt' ) );
									else if(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'compose')
										echo esc_html( __( 'Compose', 'gym_mgt' ) );
									else if(isset($_REQUEST['page']) && $_REQUEST['tab'] == 'view_message')
										echo esc_html( __( 'View Message', 'gym_mgt' ) );
									?>
								
                                    
                                    </h2>
                                </div>
                               
                            </div>
 <div class="col-md-2">
                            <ul class="list-unstyled mailbox-nav">
 								<li <?php if(!isset($_REQUEST['tab']) || ($_REQUEST['tab'] == 'inbox')){?>class="active"<?php }?>>
 									<a href="?dashboard=user&page=message&tab=inbox">
 										<i class="fa fa-inbox"></i><?php _e("Inbox","gym_mgt");?> <span class="badge badge-success pull-right">
 										<?php echo count(gmgt_count_inbox_item(get_current_user_id()));?></span>
 									</a>
 								</li>
 								<?php if($obj_gym->role == 'staff_member' || $obj_gym->role == 'accountant'){?>
                                <li <?php if(isset($_REQUEST['tab']) && $_REQUEST['tab'] == 'sentbox'){?>class="active"<?php }?>><a href="?dashboard=user&page=message&tab=sentbox"><i class="fa fa-sign-out"></i><?php _e("Sent","gym_mgt");?></a></li>
                                <?php }?>                                
                            </ul>
                        </div>
 <div class="col-md-10">
 <?php  
 	if($active_tab == 'sentbox')
 		require_once GMS_PLUGIN_DIR. '/template/message/sendbox.php';
 	if($active_tab == 'inbox')
 		require_once GMS_PLUGIN_DIR. '/template/message/inbox.php';
 	if($active_tab == 'compose')
 		require_once GMS_PLUGIN_DIR. '/template/message/composemail.php';
 	if($active_tab == 'view_message')
 		require_once GMS_PLUGIN_DIR. '/template/message/view_message.php';
 	
 	?>
 </div>
</div><!-- Main-wrapper -->
<?php ?>