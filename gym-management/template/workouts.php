<?php  $curr_user_id=get_current_user_id();
$obj_gym=new Gym_management($curr_user_id);
$obj_workouttype=new Gmgtworkouttype;
$obj_workout=new Gmgtworkout;
$active_tab = isset($_REQUEST['tab'])?$_REQUEST['tab']:'workoutlist';
?>
<script type="text/javascript">
$(document).ready(function() {
	jQuery('#workout_list').DataTable({
		"responsive": true,
		 "order": [[ 1, "asc" ]],
		 "aoColumns":[
					  {"bSortable": false},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
					 {"bSortable": false}]
		});
} );
</script>
<!-- POP up code -->
<div class="popup-bg">
    <div class="overlay-content">
    <div class="modal-content">
    <div class="invoice_data">
     </div>
     
    </div>
    </div> 
    
</div>
<!-- End POP-UP Code -->
<?php 
	
	if(isset($_POST['save_workout']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
				
			$result=$obj_workout->gmgt_add_workout($_POST);
			if($result)
			{
				wp_redirect ( home_url() . '?dashboard=user&page=workouts&tab=workoutlist&message=2');
			}
				
				
		}
		else
		{
			$exists_record=check_user_workouts($_POST['member_id'],$_POST['record_date']);
			if($exists_record==0){
					$result=$obj_workout->gmgt_add_workout($_POST);
					if($result)
					{
						wp_redirect ( home_url() . '?dashboard=user&page=workouts&tab=workoutlist&message=1');
					}
			}
			else
			{?>
						<div id="message" class="updated below-h2">
						<p><p><?php _e('Today workout All Ready Exist.','gyml_mgt');?></p></p>
						</div>
						
	  <?php }
		}
			
			
		
	}
	if(isset($_POST['save_measurement']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
		
			$result=$obj_workout->gmgt_add_measurement($_POST);
			
			?><pre><?php print_r($result); ?></pre><?php
			
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=workouts&tab=workoutlist&message=2');
			}
		
		
		}
		else
		{
			
			$result=$obj_workout->gmgt_add_measurement($_POST);
		
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=workouts&tab=workoutlist&message=1');
			}
				
		}
			
	}
	
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_workout->delete_workout($_REQUEST['daily_workout_id']);
				if($result)
				{
					wp_redirect ( home_url() . '?dashboard=user&page=workouts&tab=workoutlist&message=3');
				}
			}
		if(isset($_REQUEST['message']))
		{
			$message =$_REQUEST['message'];
			if($message == 1)
			{?>
					<div id="message" class="updated below-h2 ">
					<p>
					<?php 
						_e('Record inserted successfully','gym_mgt');
					?></p></div>
					<?php 
				
			}
			elseif($message == 2)
			{?><div id="message" class="updated below-h2 "><p><?php
						_e("Record updated successfully.",'gym_mgt');
						?></p>
						</div>
					<?php 
				
			}
			elseif($message == 3) 
			{?>
			<div id="message" class="updated below-h2"><p>
			<?php 
				_e('Record deleted successfully','gym_mgt');
			?></div></p><?php
					
			}
		}
	?>
<div class="panel-body panel-white">
<ul class="nav nav-tabs panel_tabs" role="tablist">
      <li class="<?php if($active_tab == 'workoutlist') echo "active";?>">
          <a href="?dashboard=user&page=workouts&tab=workoutlist">
             <i class="fa fa-align-justify"></i> <?php _e('Workout List', 'gym_mgt'); ?></a>
          </a>
      </li>	  
      <li class="<?php if($active_tab == 'addworkout') echo "active";?>">
      	<a href="?dashboard=user&page=workouts&tab=addworkout">
        <i class="fa fa-plus-circle"></i> <?php
		if(isset($_REQUEST['action']) && $_REQUEST['action'] =='view')
			_e('View Workout', 'gym_mgt'); 
		else
			_e('Add Workout', 'gym_mgt'); 
		?></a> 
      </li>
       
       <li class="<?php if($active_tab == 'addmeasurement') echo "active";?>">
      	<a href="?dashboard=user&page=workouts&tab=addmeasurement">
        <i class="fa fa-plus-circle"></i> <?php
		if(isset($_REQUEST['action']) && $_REQUEST['action'] =='edit')
			_e('Edit Measurement', 'gym_mgt'); 
		else
			_e('Add Measurement', 'gym_mgt'); 
		?></a> 
      </li>
</ul>






	<div class="tab-content">
		<?php if($active_tab == 'workoutlist'){?>
    	<div class="tab-pane <?php if($active_tab == 'workoutlist') echo "fade active in";?>" id="workoutlist">
		<div class="panel-body">
        <div class="table-responsive">
       <table id="workout_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php  _e( 'Photo', 'gyml_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Mobile', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Email', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
			
            </tr>
        </thead>
 
        <tfoot>
            <tr>
			<th><?php  _e( 'Photo', 'gyml_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Mobile', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Email', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
			</tr>
        </tfoot>
 
        <tbody>
         <?php //$workoutdata=$obj_workout->get_all_workout();
			if($obj_gym->role == 'member')
			{
				$user_id = get_current_user_id();
				?>
				
				<tr>
				<td class="user_image"><?php 
							$userimage=get_user_meta($user_id, 'gmgt_user_avatar', true);
						if(empty($userimage))
						{
										echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
						}
						else
							echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';
				?></td>
				<td class="membername"><a href="?page=gmgt_workout&tab=addworkout&action=view&workoutmember_id=<?php echo $user_id;?>">
				<?php $user=get_userdata($user_id);
				$display_label=$user->display_name;
				$memberid=get_user_meta($user_id,'member_id',true);
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;?></a></td>
				<td class="mobile"><?php echo get_user_meta($user_id,'mobile',true);?></td>
				<td class="email"><?php echo $user->user_email;?></td>
				
                	<td class="action"> 
					<?php if($obj_gym->role == 'staff_member' || ($obj_gym->role == 'member' && $user_id==$curr_user_id)){?>
					<a href="?dashboard=user&page=workouts&tab=addworkout&action=view&workoutmember_id=<?php echo $user_id;?>" class="btn btn-success"> <?php _e('View', 'gym_mgt' ) ;?></a>
				<a href="#" class="btn btn-default view-measurement-popup" data-val="<?php echo $user_id;?>"> <?php _e('View Measurement', 'gym_mgt' ) ;?></a>
                	<?php }?>
				</td>
               
               
            </tr>
				
				<?php 
			}
			else
			{
			
			 $get_members = array('role' => 'member');
			
			
			$membersdata=get_users($get_members);
		 if(!empty($membersdata))
		 {
		 	foreach ($membersdata as $retrieved_data){?>
            <tr>
				<td class="user_image"><?php $uid=$retrieved_data->ID;
							$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
						if(empty($userimage))
						{
										echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
						}
						else
							echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';
				?></td>
				<td class="membername"><a href="?page=gmgt_workout&tab=addworkout&action=view&workoutmember_id=<?php echo $retrieved_data->ID;?>">
				<?php $user=get_userdata($retrieved_data->ID);
				$display_label=$user->display_name;
				$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;?></a></td>
				<td class="mobile"><?php echo get_user_meta($retrieved_data->ID,'mobile',true);?></td>
				<td class="email"><?php echo $retrieved_data->user_email;?></td>
				
                	<td class="action"> 
					<?php if($obj_gym->role == 'staff_member' || ($obj_gym->role == 'member' && $retrieved_data->ID==$curr_user_id)){?>
					<a href="?dashboard=user&page=workouts&tab=addworkout&action=view&workoutmember_id=<?php echo $retrieved_data->ID;?>" class="btn btn-success"> <?php _e('View', 'gym_mgt' ) ;?></a>
				<a href="#" class="btn btn-default view-measurement-popup" data-val="<?php echo $retrieved_data->ID;?>"> <?php _e('View Measurement', 'gym_mgt' ) ;?></a>
                	<?php }?>
				</td>
               
               
            </tr>
            <?php } 
			
		}}?>
     
        </tbody>
        
        </table>
 		</div>
		</div>
	</div>
		<?php }
		if($active_tab == 'addworkout'){?>
	<script type="text/javascript">
$(document).ready(function() {
	$('#workout_form').validationEngine();
	$('#curr_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	$('#record_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	
	$(".display-members").select2();
} );
</script>
     <?php 	
	
        	
        	$daily_workout_id=0;
			if(isset($_REQUEST['daily_workout_id']))
				$daily_workout_id=$_REQUEST['daily_workout_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_workout->get_single_workout($daily_workout_id);
					
				}?>
		<div class="tab-pane <?php if($active_tab == 'addworkout') echo "fade active in";?>">
       <?php 
	   $workoutmember_id=0;
			if(isset($_REQUEST['workoutmember_id']))
				$workoutmember_id=$_REQUEST['workoutmember_id'];
				$view=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'view'){
					$view=1;?>
			<div class="panel-body"> 
		<form method="post" class="form-horizontal">  
         <div class="col-md-12">
				 		<h2><?php echo gym_get_display_name($_REQUEST['workoutmember_id']).'\'s Workout'; ?></h2>
				 		</div>
        <div class="form-group">
			<label class="col-sm-1 control-label" for="curr_date"><?php _e('Date','gym_mgt');?></label>
			<div class="col-sm-3">
			<input id="curr_date" class="form-control" type="text" value="<?php if(isset($_POST['tcurr_date'])) echo $_POST['tcurr_date']; else echo  date("Y-m-d");?>" name="tcurr_date">
			</div>
			<div class="col-sm-3">
			<input type="submit" value="<?php _e('View Workouts','gym_mgt');?>" name="view_workouts"  class="btn btn-success"/>
			</div>
		</div>
		 
          </form>
		  </div>
		  <div class="clearfix"> </div>
		   <?php 
				 if(isset($_REQUEST['view_workouts']) || isset($_REQUEST['view_workouts']))
				 {	
				 	?>
				 					 		
				 					 		<?php 
						$today_workouts=$obj_workout->get_member_today_workouts($workoutmember_id,$_POST['tcurr_date']);
				 if(!empty($today_workouts)){
						?>
						<div class="col-md-12 my-workouts-display">
						<?php foreach($today_workouts as $value){?>
						<div class='col-md-10 activity-data no-padding'>
						<div class='workout_datalist_header'>
							<h2><?php echo $value->workout_name;?></h2>
							</div>
							<div class="col-md-10 workout_datalist no-padding"> 
							<?php for($i=1;$i<=$value->sets;$i++){?>
								<div class="col-md-6 sets-row no-paddingleft">	
									<span class="text-center sets_counter"><?php echo $i;?></span>
									<span class="sets_kg"><?php echo $value->kg." Kg";?></span>								
									<span class="col-md-2 reps_count"><?php echo $value->reps;?></span>
								</div>
							<?php 
							}?>
							</div>
							</div>
							<div class="border_line"></div>
						<?php }?>
						
						</div>
						
					<?php }
						else
						{ ?>
						<span class="col-md-10"><?php _e('No Data Of Today workout','gym_mgt');?></span>
				<?php }
				}
         }
		else
		{ ?>
		
       <div class="panel-body">
        <form name="workout_form" action="" method="post" class="form-horizontal" id="workout_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="daily_workout_id" value="<?php //echo $daily_workout_id;?>"  />
		
		<?php if($obj_gym->role=='staff_member' || $obj_gym->role=='accountant'){?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?></label>
			<div class="col-sm-8">
				<?php if($view){ $member_id=$result->member_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="member_id">
				<!--<option value=""><?php _e('Select Member','gym_mgt');?></option>-->
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<?php }
		else
		{?>
			<input type="hidden" id="member_list" name="member_id" value="<?php echo $curr_user_id;?>">
	<?php } ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="record_date"><?php _e('Record Date','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="record_date" class="form-control validate[required]" type="text" userid="<?php echo get_current_user_id();?>" name="record_date" 
				value="<?php if($view){ echo $result->record_date;}elseif(isset($_POST['record_date'])){ echo $_POST['record_date'];}?>">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="workout_id"><?php _e('Workout','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8 workout_area">
			<div class='work_out_datalist'><div class='col-sm-10'><span class='col-md-10'><?php _e('Select Record Date For Today Workout','gym_mgt');?></span></div></div>
			</div>
		</div>
		
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="note"><?php _e('Note','gym_mgt');?></label>
			<div class="col-sm-8">
				<textarea id="note" class="form-control" name="note"><?php if($view){echo $result->note; }elseif(isset($_POST['note'])) echo $_POST['note']; ?> </textarea>
			</div>
		</div>
		
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($view){ _e('Save','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_workout" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php }
		}
		if($active_tab == 'addmeasurement')
		{
				$edit = 0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
						
					$edit=1;
					$result = $obj_workout->get_single_measurement($_REQUEST['measurment_id']);
						
				}
				?>
<script type="text/javascript">
$(document).ready(function() {
	    
              
	$('#workout_form').validationEngine();
	
	$(".display-members").select2();
	$('#result_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                
            }); 
} );
</script>



<div class="panel-body">
		
		<?php 
		if (isset($_REQUEST['save_measurement'])){
			
			$user_id = $_REQUEST['user_id'];
			print_r($_REQUEST['body_weight']);

			$body_weight = $_REQUEST['body_weight'];
			$body_fat = $_REQUEST['body_fat'];
			$body_mass_index = $_REQUEST['body_mass_index'];
			$push_up_max = $_REQUEST['push_up_max'];
			$crawl_max = $_REQUEST['crawl_max'];
			$plank_max = $_REQUEST['plank_max'];
			$pull_up_max = $_REQUEST['pull_up_max'];

			//Other Costom fields
			update_user_meta( $user_id, 'body_weight', $body_weight );
			update_user_meta( $user_id, 'body_fat', $body_fat );
			update_user_meta( $user_id, 'body_mass_index', $body_mass_index );
			update_user_meta( $user_id, 'push_up_max', $push_up_max );
			update_user_meta( $user_id, 'crawl_max', $crawl_max );
			update_user_meta( $user_id, 'plank_max', $plank_max );
			update_user_meta( $user_id, 'pull_up_max', $pull_up_max );
		}
		$curr_user_id = get_current_user_id();
		print_r($curr_user_id);

		$pull_up_max = get_user_meta( get_current_user_id(), 'pull_up_max');
		print_r($pull_up_max);
		?>
	<form name="workout_form" action="" method="post" class="form-horizontal" id="workout_form">
	   <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="measurment_id" value="<?php if(isset($_REQUEST['measurment_id']))echo $_REQUEST['measurment_id'];?>">
    	<?php if($obj_gym->role=='staff_member' || $obj_gym->role=='accountant'){?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$result->user_id; }elseif(isset($_REQUEST['user_id'])){$member_id=$_REQUEST['user_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="user_id">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<?php }
		else
		{?>
			<input type="hidden" id="member_list" name="user_id" value="<?php echo $curr_user_id;?>">
	<?php } ?>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result_measurment"><?php _e('Result Measurement','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
			<?php if($edit){
					$measument=$result->result_measurment;
				}
				elseif(isset($_REQUEST['result_measurment']))
				{
					$measument = $_REQUEST['result_measurment'];
				}
				else
				{
					$measument="";
				}?>
				<select name="result_measurment" class="form-control validate[required] " id="result_measurment">
				<option value=""><?php  _e('Select Result Measurement ','gym_mgt');?></option>
				<?php 	foreach(measurement_array() as $key=>$element)
						{
							
							echo '<option value='.$key.' '.selected($measument,$key).'>'.$element.'</option>';
						}
					
					?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Result','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="result" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->result;}elseif(isset($_POST['result'])) echo $_POST['result'];?>" name="result">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Body weight','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="body_weight" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->body_weight;}elseif(isset($_POST['body_weight'])) echo $_POST['body_weight'];?>" name="body_weight">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Body fat','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="body_fat" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->body_fat;}elseif(isset($_POST['body_fat'])) echo $_POST['body_fat'];?>" name="body_fat">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Body Mass Index','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="body_mass_index" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->body_mass;}elseif(isset($_POST['body_mass_index'])) echo $_POST['body_mass_index'];?>" name="body_mass_index">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>	
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Push up max','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="push_up_max" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->push_up_max;}elseif(isset($_POST['push_up_max'])) echo $_POST['push_up_max'];?>" name="push_up_max">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>	<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Crawl max','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="crawl_max" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->crawl_max;}elseif(isset($_POST['crawl_max'])) echo $_POST['crawl_max'];?>" name="crawl_max">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>	<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Plank max','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="plank_max" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->plank_max;}elseif(isset($_POST['plank_max'])) echo $_POST['plank_max'];?>" name="plank_max">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>	<div class="form-group">
			<label class="col-sm-2 control-label" for="result"><?php _e('Pull up max','gym_mgt');?> <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="pull_up_max" class="form-control validate[required,custom[number]] text-input" type="text" value="<?php if($edit){ echo $result->pull_up_max;}elseif(isset($_POST['pull_up_max'])) echo $_POST['pull_up_max'];?>" name="pull_up_max">
			</div>
			<!--<div class="col-sm-1">
				<label id="workout_mesurement" class="control-label"></label>
			</div>-->
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="result_date"><?php _e('Record Date','gym_mgt');?>  <span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="result_date" class="form-control validate[required]" type="text"  name="result_date" 
				value="<?php if($edit){ echo $result->result_date;}elseif(isset($_POST['result_date'])){ echo $_POST['result_date'];} else echo date('Y-m-d');?>">
			</div>
		</div>
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save Measurement','gym_mgt'); }else{ _e('Save Measurement','gym_mgt');}?>" name="save_measurement" class="btn btn-success"/>
        </div>
    </form>
</div>
			
	<?php } ?>
	</div>
</div>
<?php ?>