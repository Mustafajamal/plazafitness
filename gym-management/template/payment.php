<?php $curr_user_id=get_current_user_id();
$obj_class=new Gmgtclassschedule;
$obj_gym=new Gym_management($curr_user_id);
$obj_payment=new Gmgtpayment;
$active_tab = isset($_REQUEST['tab'])?$_REQUEST['tab']:'paymentlist';
 
	
	if(isset($_POST['save_product']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
			$result=$obj_payment->gmgt_add_payment($_POST);
			if($result)
			{
				wp_redirect ( home_url() . '?dashboard=user&page=payment&tab=paymentlist&message=2');
			}
				
		}
		else
		{
			$result=$obj_payment->gmgt_add_payment($_POST);
	
				if($result)
				{
					wp_redirect ( home_url() . '?dashboard=user&page=payment&tab=paymentlist&message=1');
				}
			
			}
			
			
		
	}
	
		
			if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				if(isset($_REQUEST['payment_id'])){
				$result=$obj_payment->delete_payment($_REQUEST['payment_id']);
					if($result)
					{
						wp_redirect ( home_url() . '?dashboard=user&page=payment&tab=paymentlist&message=3');
					}
				}
				if(isset($_REQUEST['income_id'])){
				$result=$obj_payment->delete_income($_REQUEST['income_id']);
					if($result)
					{
						wp_redirect ( home_url() . '?dashboard=user&page=payment&tab=incomelist&message=3');
					}
				}
				if(isset($_REQUEST['expense_id'])){
					$result=$obj_invoice->delete_expense($_REQUEST['expense_id']);
					if($result)
					{
						wp_redirect ( home_url() . '?dashboard=user&page=payment&tab=expenselist&message=3');
					}
				}
			}
	//--------save income-------------
	if(isset($_POST['save_income']))
	{
			
		if($_REQUEST['action']=='edit')
		{
				
			$result=$obj_payment->add_income($_POST);
			if($result)
			{
				wp_redirect (  home_url() . '?dashboard=user&page=payment&tab=incomelist&message=2');
			}
		}
		else
		{
			$result=$obj_payment->add_income($_POST);
			if($result)
			{
				wp_redirect (  home_url() . '?dashboard=user&page=payment&tab=incomelist&message=1');
			}
		}
			
	}		
	//--------save Expense-------------
	if(isset($_POST['save_expense']))
	{
			
		if($_REQUEST['action']=='edit')
		{
				
			$result=$obj_payment->add_expense($_POST);
			if($result)
			{
				wp_redirect (  home_url() . '?dashboard=user&page=payment&tab=expenselist&message=2');
			}
		}
		else
		{
			$result=$obj_payment->add_expense($_POST);
			if($result)
			{
				wp_redirect (  home_url() . '?dashboard=user&page=payment&tab=expenselist&message=1');
			}
		}
			
	}			
	if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>
<!-- POP up code -->
<div class="popup-bg">
    <div class="overlay-content">
    <div class="modal-content">
    <div class="invoice_data">
     </div>
     
    </div>
    </div> 
    
</div>
<!-- End POP-UP Code -->
<script type="text/javascript">
$(document).ready(function() {
	$('.date_field').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
	
	jQuery('#members_list').DataTable({
		"responsive": true,
		 "order": [[ 1, "asc" ]],
		 "aoColumns":[
	                  {"bSortable": false},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true}]
		});
		$('#member_form').validationEngine();
		$(".display-members").select2();
		
		
		
		
} );
</script>
<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
		<li class="<?php if($active_tab=='paymentlist'){?>active<?php }?>">
			<a href="?dashboard=user&page=payment&tab=paymentlist" class="tab <?php echo $active_tab == 'paymentlist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php _e('Payment List', 'gym_mgt'); ?></a>
          </a>
      </li>	 
	  <?php if($obj_gym->role=='staff_member' || $obj_gym->role=='accountant'){?>
		<li class="<?php if($active_tab=='addpayment'){?>active<?php }?>">
		 <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['payment_id']))
			{?>
		<a href="?dashboard=user&page=payment&tab=addpayment&action=edit&invoice_id=<?php if(isset($_REQUEST['invoice_id'])) echo $_REQUEST['payment_id'];?>"" class="tab <?php echo $active_tab == 'addpayment' ? 'active' : ''; ?>">
			<i class="fa fa"></i> <?php _e('Edit Payment', 'gym_mgt'); ?></a><?php }
			else{?>
				<a href="?dashboard=user&page=payment&tab=addpayment" class="tab <?php echo $active_tab == 'addpayment' ? 'active' : ''; ?>">
				<i class="fa fa-plus-circle"></i> <?php _e('Add Payment', 'gym_mgt'); ?></a>
			<?php }?>
        
      </li>
	  <li class="<?php if($active_tab=='incomelist'){?>active<?php }?>">
			<a href="?dashboard=user&page=payment&tab=incomelist" class="tab <?php echo $active_tab == 'incomelist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php _e('Income List', 'gym_mgt'); ?></a>
          </a>
      </li>
       <li class="<?php if($active_tab=='addincome'){?>active<?php }?>">
		  <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['income_id']))
			{?>
			<a href="?dashboard=user&page=payment&tab=addincome&action=edit&income_id=<?php if(isset($_REQUEST['income_id'])) echo $_REQUEST['income_id'];?>"" class="tab <?php echo $active_tab == 'addincome' ? 'active' : ''; ?>">
             <i class="fa fa"></i> <?php _e('Edit Income', 'gym_mgt'); ?></a>
			 <?php }
			else
			{?>
				<a href="?dashboard=user&page=payment&tab=addincome" class="tab <?php echo $active_tab == 'addincome' ? 'active' : ''; ?>">
				<i class="fa fa-plus-circle"></i> <?php _e('Add Income', 'gym_mgt'); ?></a>
	  <?php } ?>
	  </li>
	  <li class="<?php if($active_tab=='expenselist'){?>active<?php }?>">
			<a href="?dashboard=user&page=payment&tab=expenselist" class="tab <?php echo $active_tab == 'expenselist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php _e('Expense List', 'gym_mgt'); ?></a>
          </a>
      </li>
       <li class="<?php if($active_tab=='addexpense'){?>active<?php }?>">
		  <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['income_id']))
			{?>
			<a href="?dashboard=user&page=payment&tab=addexpense&action=edit&expense_id=<?php if(isset($_REQUEST['income_id'])) echo $_REQUEST['income_id'];?>"" class="tab <?php echo $active_tab == 'addexpense' ? 'active' : ''; ?>">
             <i class="fa fa"></i> <?php _e('Edit Expense', 'gym_mgt'); ?></a>
			 <?php }
			else
			{?>
				<a href="?dashboard=user&page=payment&tab=addexpense" class="tab <?php echo $active_tab == 'addexpense' ? 'active' : ''; ?>">
				<i class="fa fa-plus-circle"></i> <?php _e('Add Expense', 'gym_mgt'); ?></a>
	  <?php } ?>
	  
	</li>
	  <?php }?>
	  
	  
	   
	  
</ul>
	<div class="tab-content">
	<?php if($active_tab == 'paymentlist')
	{ ?>

		
		 <script type="text/javascript">
$(document).ready(function() {
	jQuery('#payment_list').DataTable({
		"responsive": true,
		"order": [[ 2, "Desc" ]],
		 "aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                 <?php if($obj_gym->role == 'accountant'){?>
					{"bSortable": false} <?php }?>
	               ]
		});
} );
</script>
    
    
        <div class="panel-body">
		
		<form name="gmgt_payment" action="" method="post">
        	<div class="table-responsive">
        <table id="payment_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
			<th><?php  _e( 'Title', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Total Amount', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Payment Date', 'gym_mgt' ) ;?></th>
			 <?php if($obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
			<th><?php  _e( 'Title', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Total Amount', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Payment Date', 'gym_mgt' ) ;?></th>
			 <?php if($obj_gym->role == 'accountant'){?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
			<?php }?>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
			if($obj_gym->role=='member')
				$paymentdata=$obj_payment->get_own_payment($curr_user_id);
			else
				$paymentdata=$obj_payment->get_all_payment();
		
			
		 if(!empty($paymentdata))
		 {
			foreach ($paymentdata as $retrieved_data){?>
            <tr>
				<td class="productname">
				<?php if($obj_gym->role == 'accountant'){?>
				<a href="?dashboard=user&page=payment&tab=addpayment&action=edit&payment_id=<?php echo $retrieved_data->payment_id;?>" ><?php echo $retrieved_data->title;?></a>
				<?php }
				else
				{?><a href="#" ><?php echo $retrieved_data->title;?></a>
				<?php }?></td>
				<td class="paymentby"><?php $user=get_userdata($retrieved_data->member_id);
					$memberid=get_user_meta($retrieved_data->member_id,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;?></td>
				<td class="totalamount"><?php echo $retrieved_data->total_amount;?></td>
				<td class="paymentdate"><?php echo $retrieved_data->payment_date;?></td>
                 <?php if($obj_gym->role == 'accountant')
                   {?>
               	<td class="action">
				<a  href="#" class="show-invoice-popup btn btn-default" idtest="<?php echo $retrieved_data->payment_id; ?>" invoice_type="invoice">
				<i class="fa fa-eye"></i> <?php _e('View Invoice', 'gym_mgt');?></a>

               	<a href="?dashboard=user&page=payment&tab=addpayment&action=edit&payment_id=<?php echo $retrieved_data->payment_id?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?dashboard=user&page=payment&tab=paymentlist&action=delete&payment_id=<?php echo $retrieved_data->payment_id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                
                </td>
				   <?php }?>
            </tr>
            <?php } 
			
		}?>
     
        </tbody>
        </table>
 		</div>
		</form>
		</div>
		
	<?php } 
	if($active_tab == 'addpayment')
	{ ?>
 		
		<!--Add Payment information-->
		

<script type="text/javascript">
$(document).ready(function() {
	$('#payment_form').validationEngine();
	$('#due_date').datepicker({dateFormat : 'yy-mm-dd',
		  changeMonth: true,
	        changeYear: true,
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
     <?php 	
	
        	$payment_id=0;
			if(isset($_REQUEST['payment_id']))
				$payment_id=$_REQUEST['payment_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_payment->get_single_payment($payment_id);
					
				}?>
		
       <div class="panel-body">	
         <form name="payment_form" action="" method="post" class="form-horizontal" id="payment_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="payment_id" value="<?php echo $payment_id;?>">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">	
				<?php if($edit){ $member_id=$result->member_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="member_id">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="title"><?php _e('Title','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="payment_title" class="form-control validate[custom[onlyLetterSp]] text-input" type="text" value="<?php if($edit){ echo $result->title;}elseif(isset($_POST['payment_title'])) echo $_POST['payment_title'];?>" name="payment_title">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="due_date"><?php _e('Due Date','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="due_date" class="form-control" type="text"  name="due_date" 
				value="<?php if($edit){ echo $result->due_date;}elseif(isset($_POST['due_date'])){ echo $_POST['due_date'];}?>">
			</div>
		</div>
		<!--  
		<div class="form-group">
			<label class="col-sm-2 control-label" for="amount"><?php _e('Amount','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="amount" class="form-control validate[required,custom[number]]" type="text" value="<?php if($edit){ echo $result->unit_price;}?>" name="amount">
			</div>
		</div>
		-->
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="discount"><?php _e('Discount Amount','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="discount" class="form-control validate[custom[number]]" type="text" value="<?php if($edit){ echo $result->discount;}?>" name="discount">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="total_amount"><?php _e('Total Amount','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="total_amount" class="form-control validate[required,custom[number]]" type="text" value="<?php if($edit){ echo $result->total_amount;}?>" name="total_amount">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="payment_status"><?php _e('Status','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="payment_status" id="payment_status" class="form-control">
					<option value="Paid"
						<?php if($edit)selected('Paid',$result->payment_status);?> class="validate[required]"><?php _e('Paid','gym_mgt');?></option>
					<option value="Part Paid"
						<?php if($edit)selected('Part Paid',$result->payment_status);?> class="validate[required]"><?php _e('Part Paid','gym_mgt');?></option>
						<option value="Unpaid"
						<?php if($edit)selected('Unpaid',$result->payment_status);?> class="validate[required]"><?php _e('Unpaid','gym_mgt');?></option>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="description"><?php _e('Description','gym_mgt');?></label>
			<div class="col-sm-8">
				<textarea name="description" id="description" class="form-control"><?php if($edit){ echo $result->description;}?></textarea>
			</div>
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_product" class="btn btn-success"/>
        </div>
	   
	   
        </form>
        </div>
		

	<?php }
	if($active_tab == 'incomelist')
	{?>
		<!--Income information-->
		


<script type="text/javascript">
$(document).ready(function() {
	jQuery('#tblincome').DataTable({
		"responsive": true,
		 "order": [[ 1, "Desc" ]],
		 "aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true}, 
					   <?php if($obj_gym->role == 'accountant'){?>
					{"bSortable": false} <?php }?>
	               ]
		});
		
	
} );
</script>
     <div class="panel-body">
        	<div class="table-responsive">
        <table id="tblincome" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
				<th> <?php _e( 'Member Name', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Amount', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Date', 'gym_mgt' ) ;?></th>
                 <?php if($obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </thead>
		<tfoot>
            <tr>
				<th> <?php _e( 'Member Name', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Amount', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Date', 'gym_mgt' ) ;?></th>
                <?php if($obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
			$paymentdata=$obj_payment->get_all_income_data();
		 	foreach ($paymentdata as $retrieved_data){ 
				$all_entry=json_decode($retrieved_data->entry);
				$total_amount=0;
				foreach($all_entry as $entry){
					$total_amount+=$entry->amount;
				}?>
            <tr>
				<td class="member_name"><?php $user=get_userdata($retrieved_data->supplier_name);
					$memberid=get_user_meta($retrieved_data->supplier_name,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;?></td>
				<td class="income_amount"><?php echo $total_amount;?></td>
                <td class="status"><?php echo $retrieved_data->invoice_date;?></td>
                 <?php if($obj_gym->role == 'accountant')
                   {?>
               	<td class="action">
				<a  href="#" class="show-invoice-popup btn btn-default" idtest="<?php echo $retrieved_data->invoice_id; ?>" invoice_type="income">
				<i class="fa fa-eye"></i> <?php _e('View Income', 'gym_mgt');?></a>
				<a href="?dashboard=user&page=payment&tab=addincome&action=edit&income_id=<?php echo $retrieved_data->invoice_id;?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?dashboard=user&page=payment&tab=incomelist&action=delete&income_id=<?php echo $retrieved_data->invoice_id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                </td>
				  <?php } ?>
            </tr>
            <?php } 
			
		?>
     
        </tbody>
        
        </table>
        </div>
        </div>
	
	<?php } 
	if($active_tab == 'addincome')
	{?>
		<!--Add Income information-->
		

<script type="text/javascript">
$(document).ready(function() {
	$('#income_form').validationEngine();
	$('#invoice_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
		
} );
</script>
	
     <?php 
	 $income_id=0;
			if(isset($_REQUEST['income_id']))
				$income_id=$_REQUEST['income_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_payment->gmgt_get_income_data($income_id);
					//var_dump($result);
				
				}?>
		
       <div class="panel-body">
	 
        <form name="income_form" action="" method="post" class="form-horizontal" id="income_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="income_id" value="<?php echo $income_id;?>">
		<input type="hidden" name="invoice_type" value="income">
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$result->supplier_name; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="supplier_name">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="invoice_label"><?php _e('Income label','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="invoice_label" class="form-control validate[required,custom[onlyLetterSp]] text-input" type="text" value="<?php if($edit){ echo $result->invoice_label;}elseif(isset($_POST['invoice_label'])) echo $_POST['payment_title'];?>" name="invoice_label">
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="payment_status"><?php _e('Status','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="payment_status" id="payment_status" class="form-control validate[required]">
					<option value="Paid"
						<?php if($edit)selected('Paid',$result->payment_status);?> ><?php _e('Paid','gym_mgt');?></option>
					<option value="Part Paid"
						<?php if($edit)selected('Part Paid',$result->payment_status);?>><?php _e('Part Paid','gym_mgt');?></option>
						<option value="Unpaid"
						<?php if($edit)selected('Unpaid',$result->payment_status);?>><?php _e('Unpaid','gym_mgt');?></option>
			</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="invoice_date"><?php _e('Date','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="invoice_date" class="form-control " type="text"  value="<?php if($edit){ echo $result->invoice_date;}elseif(isset($_POST['invoice_date'])){ echo $_POST['invoice_date'];}else{ echo date("Y-m-d");}?>" name="invoice_date">
			</div>
		</div>
		<hr>
		
		<?php 
		
			if($edit){
				$all_income_entry=json_decode($result->entry);
			
			}
			else
			{
				if(isset($_POST['income_entry'])){
					
					$all_data=$obj_invoice->get_entry_records($_POST);
					$all_income_entry=json_decode($all_data);
						
				}
				
					
			}
			
			
			if(!empty($all_income_entry))
			{
					foreach($all_income_entry as $entry){
					?>
					<div id="income_entry">
						<div class="form-group">
						<label class="col-sm-2 control-label" for="income_entry"><?php _e('Income Entry','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-2">
							<input id="income_amount" class="form-control validate[required] text-input" type="text" value="<?php echo $entry->amount;?>" name="income_amount[]">
						</div>
						<div class="col-sm-4">
							<input id="income_entry" class="form-control validate[required] text-input" type="text" value="<?php echo $entry->entry;?>" name="income_entry[]">
						</div>
						
						<div class="col-sm-2">
						<button type="button" class="btn btn-default" onclick="deleteParentElement(this)">
						<i class="entypo-trash"><?php _e('Delete','gym_mgt');?></i>
						</button>
						</div>
						</div>	
					</div>
					<?php }
				
			}
			else
			{?>
					<div id="income_entry">
						<div class="form-group">
						<label class="col-sm-2 control-label" for="income_entry"><?php _e('Income Entry','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-2">
							<input id="income_amount" class="form-control validate[required] text-input" type="text" value="" name="income_amount[]" placeholder="Income Amount">
						</div>
						<div class="col-sm-4">
							<input id="income_entry" class="form-control validate[required] text-input" type="text" value="" name="income_entry[]" placeholder="Income Entry Label">
						</div>						
						<div class="col-sm-2">
						<button type="button" class="btn btn-default" onclick="deleteParentElement(this)">
						<i class="entypo-trash"><?php _e('Delete','gym_mgt');?></i>
						</button>
						</div>
						</div>	
					</div>
					
		<?php }?>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="income_entry"></label>
			<div class="col-sm-3">
				
				<button id="add_new_entry" class="btn btn-default btn-sm btn-icon icon-left" type="button"   name="add_new_entry" onclick="add_entry()"><?php _e('Add Income Entry','gym_mgt'); ?>
				</button>
			</div>
		</div>
		<hr>
		<div class="col-sm-offset-2 col-sm-8">
        	<input type="submit" value="<?php if($edit){ _e('Save Income','gym_mgt'); }else{ _e('Create Income Entry','gym_mgt');}?>" name="save_income" class="btn btn-success"/>
        </div>
        </form>
        </div>
       <script>
  
   	// CREATING BLANK INVOICE ENTRY
   	var blank_income_entry ='';
   	$(document).ready(function() { 
   		blank_income_entry = $('#income_entry').html();
   		//alert("hello" + blank_invoice_entry);
   	}); 

   	function add_entry()
   	{
   		$("#income_entry").append(blank_income_entry);
   		//alert("hellooo");
   	}
   	
   	// REMOVING INVOICE ENTRY
   	function deleteParentElement(n){
   		n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
   	}
       </script> 
		
	<?php }
	if($active_tab == 'expenselist')
	{ ?>

<script type="text/javascript">
$(document).ready(function() {
	jQuery('#tblexpence').DataTable({
		"responsive": true,
		 "order": [[ 2, "Desc" ]],
		 "aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                 <?php if($obj_gym->role == 'accountant'){?>
					{"bSortable": false} <?php }?>
	               ]
		});
		
	
} );
</script>
     <div class="panel-body">
        	<div class="table-responsive">
        <table id="tblexpence" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
				<th> <?php _e( 'Supplier Name', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Amount', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Date', 'gym_mgt' ) ;?></th>
                 <?php if($obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </thead>
		<tfoot>
            <tr>
				<th> <?php _e( 'Supplier Name', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Amount', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Date', 'gym_mgt' ) ;?></th>
                 <?php if($obj_gym->role == 'accountant')
                   {?>
			<th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
				   <?php }?>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
		
		 	foreach ($obj_payment->get_all_expense_data() as $retrieved_data){ 
				$all_entry=json_decode($retrieved_data->entry);
				$total_amount=0;
				foreach($all_entry as $entry){
					$total_amount+=$entry->amount;
				}
		 ?>
            <tr>
				
				<td class="party_name"><?php echo $retrieved_data->supplier_name;?></td>
				<td class="income_amount"><?php echo $total_amount;?></td>
                <td class="status"><?php echo $retrieved_data->invoice_date;?></td>
                 <?php if($obj_gym->role == 'accountant')
                   {?>
               	<td class="action">
				<a  href="#" class="show-invoice-popup btn btn-default" idtest="<?php echo $retrieved_data->invoice_id; ?>" invoice_type="expense">
				<i class="fa fa-eye"></i> <?php _e('View Expense', 'gym_mgt');?></a>
				<a href="?dashboard=user&page=payment&tab=addexpense&action=edit&expense_id=<?php echo $retrieved_data->invoice_id;?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?dashboard=user&page=payment&tab=expenselist&action=delete&expense_id=<?php echo $retrieved_data->invoice_id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                </td>
				   <?php } ?>
            </tr>
            <?php } 
			
		?>
     
        </tbody>
        
        </table>
        </div>
        </div>	

	<?php } 
	if($active_tab == 'addexpense')
	{ ?>	

<?php 
	//This is Dashboard at admin side
	$obj_payment= new Gmgtpayment();?>
	<script type="text/javascript">

$(document).ready(function() {
	$('#expense_form').validationEngine();
	$('#invoice_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
		
} );
</script>
	
     <?php 	

	
        	$expense_id=0;
			if(isset($_REQUEST['expense_id']))
				$expense_id=$_REQUEST['expense_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_payment->gmgt_get_income_data($expense_id);
					//var_dump($result);
					
				}?>
		
       <div class="panel-body">
        <form name="expense_form" action="" method="post" class="form-horizontal" id="expense_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="expense_id" value="<?php echo $expense_id;?>">
		<input type="hidden" name="invoice_type" value="expense">
		
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="patient"><?php _e('Supplier Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="supplier_name" class="form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $result->supplier_name;}elseif(isset($_POST['supplier_name'])) echo $_POST['supplier_name'];?>" name="supplier_name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="payment_status"><?php _e('Status','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="payment_status" id="payment_status" class="form-control validate[required]">
					<option value="Paid"
						<?php if($edit)selected('Paid',$result->payment_status);?> ><?php _e('Paid','gym_mgt');?></option>
					<option value="Part Paid"
						<?php if($edit)selected('Part Paid',$result->payment_status);?>><?php _e('Part Paid','gym_mgt');?></option>
						<option value="Unpaid"
						<?php if($edit)selected('Unpaid',$result->payment_status);?>><?php _e('Unpaid','gym_mgt');?></option>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="invoice_date"><?php _e('Date','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="invoice_date" class="form-control validate[required]" type="text"  value="<?php if($edit){ echo $result->invoice_date;}elseif(isset($_POST['invoice_date'])){ echo $_POST['invoice_date'];}else{ echo date("Y-m-d");}?>" name="invoice_date">
			</div>
		</div>
		<hr>
		
		<?php 
			
			if($edit){
				$all_expense_entry=json_decode($result->entry);
			}
			else
			{
				if(isset($_POST['income_entry'])){
					
					$all_data=$obj_payment->get_entry_records($_POST);
					$all_expense_entry=json_decode($all_data);
				}
				
					
			}
			if(!empty($all_expense_entry))
			{
					foreach($all_expense_entry as $entry){
					?>
					<div id="expense_entry">
						<div class="form-group">
						<label class="col-sm-2 control-label" for="income_entry"><?php _e('Expense Entry','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-2">
							<input id="income_amount" class="form-control validate[required] text-input" type="text" value="<?php echo $entry->amount;?>" name="income_amount[]" >
						</div>
						<div class="col-sm-4">
							<input id="income_entry" class="form-control validate[required] text-input" type="text" value="<?php echo $entry->entry;?>" name="income_entry[]">
						</div>
						
						<div class="col-sm-2">
						<button type="button" class="btn btn-default" onclick="deleteParentElement(this)">
						<i class="entypo-trash"><?php _e('Delete','gym_mgt');?></i>
						</button>
						</div>
						</div>	
					</div>
					<?php }
				
			}
			else
			{?>
					<div id="expense_entry">
						<div class="form-group">
						<label class="col-sm-2 control-label" for="income_entry"><?php _e('Expense Entry','gym_mgt');?><span class="require-field">*</span></label>
						<div class="col-sm-2">
							<input id="income_amount" class="form-control validate[required] text-input" type="text" value="" name="income_amount[]" placeholder="Expense Amount">
						</div>
						<div class="col-sm-4">
							<input id="income_entry" class="form-control validate[required] text-input" type="text" value="" name="income_entry[]" placeholder="Expense Entry Label">
						</div>
						
						<div class="col-sm-2">
						<button type="button" class="btn btn-default" onclick="deleteParentElement(this)">
						<i class="entypo-trash"><?php _e('Delete','gym_mgt');?></i>
						</button>
						</div>
						</div>	
					</div>
					
		<?php }?>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="expense_entry"></label>
			<div class="col-sm-3">
				
				<button id="add_new_entry" class="btn btn-default btn-sm btn-icon icon-left" type="button"   name="add_new_entry" onclick="add_entry()"><?php _e('Add Expense Entry','gym_mgt'); ?>
				</button>
			</div>
		</div>
		<hr>
		<div class="col-sm-offset-2 col-sm-8">
        	<input type="submit" value="<?php if($edit){ _e('Save Expense','gym_mgt'); }else{ _e('Create Expense Entry','gym_mgt');}?>" name="save_expense" class="btn btn-success"/>
        </div>
        </form>
        </div>
       <script>

   
   
   	
  
   	// CREATING BLANK INVOICE ENTRY
   	var blank_income_entry ='';
   	$(document).ready(function() { 
   		blank_expense_entry = $('#expense_entry').html();
   		//alert("hello" + blank_invoice_entry);
   	}); 

   	function add_entry()
   	{
   		$("#expense_entry").append(blank_expense_entry);
   		//alert("hellooo");
   	}
   	
   	// REMOVING INVOICE ENTRY
   	function deleteParentElement(n){
   		n.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode);
   	}
       </script> 

<?php }?>
</div>
</div>
<?php ?>