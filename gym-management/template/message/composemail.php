<?php
if(isset($_POST['save_message']))
{

	$created_date = date("Y-m-d H:i:s");
	$subject = $_POST['subject'];
	$message_body = $_POST['message_body'];
	$created_date = date("Y-m-d H:i:s");
	$tablename="gmgt_message";

	$role=$_POST['receiver'];
	if(isset($_REQUEST['class_id']))
	$class_id = $_REQUEST['class_id'];
	if($role == 'member' || $role == 'staff_member' || $role == 'accountant')
	{
		
		$userdata=gmgt_get_user_notice($role,$_REQUEST['class_id']);
		
		if(!empty($userdata))
		{
			
				$mail_id = array();
				$i = 0;
					foreach($userdata as $user)
					{
						
						if($role == 'parent' && $class_id != 'all')
						$mail_id[]=$user['ID'];
						else 
							$mail_id[]=$user->ID;
						
						$i++;
					}


				foreach($mail_id as $user_id)
				{

					$reciever_id = $user_id;
					$message_data=array('sender'=>get_current_user_id(),
							'receiver'=>$user_id,
							'subject'=>$subject,
							'message_body'=>$message_body,
							'date'=>$created_date,
							'status' =>0
					);
					gmgt_insert_record($tablename,$message_data);


						
						
						
				}
				$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body

				) );


				$result=add_post_meta($post_id, 'message_for',$role);
				$result=add_post_meta($post_id, 'gmgt_class_id',$_REQUEST['class_id']);

		}
		else
		{
			
				$user_id =$_POST['receiver'];
				$message_data=array('sender'=>get_current_user_id(),
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0
				);
				gmgt_insert_record($tablename,$message_data);
			
			$post_id = wp_insert_post( array(
					'post_status' => 'publish',
					'post_type' => 'message',
					'post_title' => $subject,
					'post_content' =>$message_body
			
			) );
			
			
			$result=add_post_meta($post_id, 'message_for','user');
			$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);

		}
	}
	else
	{
				$user_id =$_POST['receiver'];
				$message_data=array('sender'=>get_current_user_id(),
						'receiver'=>$user_id,
						'subject'=>$subject,
						'message_body'=>$message_body,
						'date'=>$created_date,
						'status' =>0
				);
				gmgt_insert_record($tablename,$message_data);
			
				$post_id = wp_insert_post( array(
						'post_status' => 'publish',
						'post_type' => 'message',
						'post_title' => $subject,
						'post_content' =>$message_body
				
				) );
			
			
			$result=add_post_meta($post_id, 'message_for','user');
			$result=add_post_meta($post_id, 'message_gmgt_user_id',$user_id);
	}
	
}
if(isset($result))
{?>
	<div id="message" class="updated below-h2">
		<p><?php _e('Message Sent Successfully!','school-mgt');?></p>
	</div>
<?php }	
	

?>
<script type="text/javascript">

$(document).ready(function() {
	 $('#message_form').validationEngine();
} );
</script>
		<div class="mailbox-content">
		<h2>
        	 	<?php  $edit=0;
			 if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit')
					{
						 echo esc_html( __( 'Edit Message', 'school-mgt') );
						 $edit=1;
						 $exam_data= get_exam_by_id($_REQUEST['exam_id']);
					}
					?>
        </h2>
        <?php
		if(isset($message))
			echo '<div id="message" class="updated below-h2"><p>'.$message.'</p></div>';
		?>
        <form name="class_form" action="" method="post" class="form-horizontal" id="message_form">
          <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
        <div class="form-group">
                                              <label class="col-sm-2 control-label" for="to"><?php _e('Message To','gym_mgt');?><span class="require-field">*</span></label>
                                            <div class="col-sm-8">
                                                <select name="receiver" class="form-control validate[required] text-input" id="to">
                           
							<option value="member"><?php _e('Members','gym_mgt');?></option>	
							<option value="staff_member"><?php _e('Staff Members','gym_mgt');?></option>	
							<option value="accountant"><?php _e('Accountant','gym_mgt');?></option>	
							
							<?php echo gmgt_get_all_user_in_message();?>
						</select>
                                            </div>
                                        </div>
         <div id="smgt_select_class">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="sms_template"><?php _e('Select Class','school-mgt');?></label>
			<div class="col-sm-8">
			
				 <select name="class_id"  id="class_list" class="form-control">
                	<option value="all"><?php _e('All','school-mgt');?></option>
                    <?php
					  foreach(gmgt_get_allclass() as $classdata)
					  {  
					  ?>
					   <option  value="<?php echo $classdata['class_id'];?>" ><?php echo $classdata['class_name'];?></option>
				 <?php }?>
                </select>
			</div>
		</div>
		</div>
         <div class="form-group">
                                            <label class="col-sm-2 control-label" for="subject"><?php _e('Subject','school-mgt');?><span class="require-field">*</span></label>
                                            <div class="col-sm-8">
                                               <input id="subject" class="form-control validate[required]" type="text" name="subject" value="<?php if($edit){ echo $exam_data->exam_date;}?>">
                                            </div>
                                        </div>
          <div class="form-group">
                                            <label class="col-sm-2 control-label" for="subject"><?php _e('Message Comment','school-mgt');?></label>
                                            <div class="col-sm-8">
                                              <textarea name="message_body" id="message_body" class="form-control "><?php if($edit){ echo $exam_data->exam_comment;}?></textarea>
                                            </div>
                                        </div>
           
			
		   <div class="form-group">
		   
                                           
                                           <div class="col-sm-10 ">
                                          
                                            <div class="pull-right">
                                            <input type="submit" value="<?php if($edit){ _e('Save Message','school-mgt'); }else{ _e('Send Message','school-mgt');}?>" name="save_message" class="btn btn-success"/>
                                            </div>
                                            </div>
                                        </div>
         	
        
        </form>
        
        </div>
<?php

?>