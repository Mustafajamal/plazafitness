<?php $curr_user_id=get_current_user_id();
$obj_gym=new Gym_management($curr_user_id);
$obj_nutrition=new Gmgtnutrition;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'nutritionlist';


	if(isset($_POST['save_nutrition']))
	{
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='edit')
		{
				
			$result=$obj_nutrition->gmgt_add_nutrition($_POST);
			if($result)
			{
				wp_redirect ( home_url().'?dashboard=user&page=nutrition&tab=nutritionlist&message=2');
			}
				
				
		}
		else
		{
			$result=$obj_nutrition->gmgt_add_nutrition($_POST);
	
				if($result)
				{
					wp_redirect ( home_url().'?dashboard=user&page=nutrition&tab=nutritionlist&message=1');
				}
			
			}
			
			
		
	}
	
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_nutrition->delete_nutrition($_REQUEST['nutrition_id']);
				if($result)
				{
					wp_redirect ( home_url().'?dashboard=user&page=nutrition&tab=nutritionlist&message=3');
				}
			}
		if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>

<script type="text/javascript">
$(document).ready(function() {
	jQuery('#nutrition_list').DataTable({
		"responsive": true,
		"order": [[ 0, "asc" ]],
		"aoColumns":[
					{"bSortable": false},
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": false}]
		});
		$('#nutrition_form').validationEngine();
		$(".display-members").select2();
		$('.datepicker').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
} );
</script>
<!-- POP up code -->
<div class="popup-bg">
    <div class="overlay-content">
    <div class="modal-content">
    <div class="category_list">
     </div>
     
    </div>
    </div> 
    
</div>
<!-- End POP-UP Code -->

<div class="panel-body panel-white">
 <ul class="nav nav-tabs panel_tabs" role="tablist">
     
	  
	  	<li class="<?php if($active_tab=='nutritionlist'){?>active<?php }?>">
			<a href="?dashboard=user&page=nutrition&tab=nutritionlist" class="tab <?php echo $active_tab == 'nutritionlist' ? 'active' : ''; ?>">
             <i class="fa fa-align-justify"></i> <?php _e('Nutrition Schedule List', 'gym_mgt'); ?></a>
          </a>
      </li>
	  <?php if($obj_gym->role=='staff_member'){?>
       <li class="<?php if($active_tab=='addnutrition'){?>active<?php }?>">
		  <?php  if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'view')
			{?>
			<a href="?dashboard=user&page=nutrition&tab=addnutrition&&action=view&workoutmember_id=<?php echo $_REQUEST['workoutmember_id'];?>" class="nav-tab <?php echo $active_tab == 'addnutrition' ? 'nav-tab-active' : ''; ?>">
             <i class="fa fa"></i> <?php _e('View  Nutrition Schedule', 'gym_mgt'); ?></a>
			 <?php }
			else
			{?>
				<a href="?dashboard=user&page=nutrition&tab=addnutrition" class="tab <?php echo $active_tab == 'addnutrition' ? 'active' : ''; ?>">
				<i class="fa fa-plus-circle"></i> <?php _e('Add Nutrition Schedule', 'gym_mgt'); ?></a>
	  <?php } ?>
	  
	</li>
	  <?php }?>
</ul>

	<div class="tab-content">
	<?php if($active_tab == 'nutritionlist')
	{ ?>	
    	 
    <form name="wcwm_report" action="" method="post">
    
        <div class="panel-body">
         <?php if($obj_gym->role=='member'){
         	
         	$nutrition_logdata=get_user_nutrition(get_current_user_id());
         	if(isset($nutrition_logdata))
         		foreach($nutrition_logdata as $row){
         		$all_logdata=get_nutritiondata($row->id); //var_dump($workout_logdata);
         	
         		$arranged_workout=set_nutrition_array($all_logdata);
         	
         		?>
         		 				<div class="workout_<?php echo $row->id;?> workout-block">
         		 				<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo "Start From <span class='work_date'>".$row->start_date."</span> To <span class='work_date'>".$row->expire_date; ?></h3>						
					</div>
         		 				
         		 				
         		 				
         		 				<div class="panel panel-white">
         		 					
         		 					<?php
         		 					if(!empty($arranged_workout))
         		 					{
         		 					?>
         		 					<div class="work_out_datalist_header">
         		 					<div class="col-md-4 col-sm-4 col-xs-4">  
         		 					<strong><?php _e('Day Name','gym_mgt');?></strong>
         		 					</div>
         		 					<div class="col-md-8 col-sm-8 col-xs-8">
         		 					<span class="col-md-3 hidden-xs"><?php _e('Time','gym_mgt');?></span>
         		 					<span class="col-md-3"><?php _e('Description','gym_mgt');?></span>
         		 					
         		 					</div>
         		 					</div>
         		 					<?php 
         		 					foreach($arranged_workout as $key=>$rowdata){?>
         		 				<div class="work_out_datalist">
         		 				<div class="col-md-4 col-sm-4 col-xs-12 day_name">  
         		 					<?php echo $key;?>
         		 				</div>
         		 				<div class="col-md-8 col-sm-8 col-xs-12">
         		 						<?php foreach($rowdata as $row){
         		 								echo $row."<br>";
         		 						} ?>
         		 				</div>
         		 				</div>
         		 				<?php } 
         		 					}?>
         		 				
         		 				</div>
         		 			</div>
         		 			<?php }	
         }else{?>
        	<div class="table-responsive">
       <table id="nutrition_list" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
				<th><?php  _e( 'Photo', 'gyml_mgt' ) ;?></th>
				<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Goal', 'gym_mgt' ) ;?></th>
			
               <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
				<th><?php  _e( 'Photo', 'gyml_mgt' ) ;?></th>
				<th><?php  _e( 'Member Name', 'gym_mgt' ) ;?></th>
			<th><?php  _e( 'Member Goal', 'gym_mgt' ) ;?></th>
			
               <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </tfoot>
 
        <tbody>
         <?php
		$get_members = array('role' => 'member');
			$membersdata=get_users($get_members);
		 if(!empty($membersdata))
		 {
		 	foreach ($membersdata as $retrieved_data){?>
            <tr>
				<td class="user_image"><?php $uid=$retrieved_data->ID;
							$userimage=get_user_meta($uid, 'gmgt_user_avatar', true);
						if(empty($userimage))
						{
										echo '<img src='.get_option( 'gmgt_system_logo' ).' height="50px" width="50px" class="img-circle" />';
						}
						else
							echo '<img src='.$userimage.' height="50px" width="50px" class="img-circle"/>';
				?></td>
				<td class="member"><a href="?page=gmgt_workouttype&tab=addworkouttype&action=edit&workoutmember_id=<?php echo $retrieved_data->ID;?>">
				<?php $user=get_userdata($retrieved_data->ID);
				$display_label=$user->display_name;
				$memberid=get_user_meta($retrieved_data->ID,'member_id',true);
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;?></a></td>
				<td class="member-goal"><?php $intrestid=get_user_meta($retrieved_data->ID,'intrest_area',true);
				echo get_the_title($intrestid);?></td>			
				<td class="action"> 
				<a href="?dashboard=user&page=nutrition&tab=addnutrition&action=view&workoutmember_id=<?php echo $retrieved_data->ID;?>" class="btn btn-default">
				<i class="fa fa-eye"></i> <?php _e('View Nutrition', 'gym_mgt');?></a>
				
               
                
                </td>
               
            </tr>
            <?php } 
			
		}?>
     
        </tbody>             
        
        </table>
        </div>
        <?php }?>
        </div>
       
</form>		<?php 
	}
	if($active_tab == 'addnutrition')
	 {
	 	$nutrition_id=0;
	 	$edit=0;
	 	if(isset($_REQUEST['workouttype_id']))
	 		$workouttype_id=$_REQUEST['workouttype_id'];
	 	if(isset($_REQUEST['workoutmember_id'])){
	 		$edit=0;
	 		$workoutmember_id=$_REQUEST['workoutmember_id'];
	 	
	 		$nutrition_logdata=get_user_nutrition($workoutmember_id);
	 	
	 	}?>
		
       <div class="panel-body">
        <form name="nutrition_form" action="" method="post" class="form-horizontal" id="nutrition_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="nutrition_id" value="<?php echo $nutrition_id;?>"  />
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Member','gym_mgt');?><span class="require-field">*</span></label>	
			<div class="col-sm-8">
				<?php if($edit){ $member_id=$workoutmember_id; }elseif(isset($_POST['member_id'])){$member_id=$_POST['member_id'];}else{$member_id='';}?>
				<select id="member_list" class="display-members" name="member_id" required="true">
				<option value=""><?php _e('Select Member','gym_mgt');?></option>
					<?php $get_members = array('role' => 'member');
					$membersdata=get_users($get_members);
					 if(!empty($membersdata))
					 {
						foreach ($membersdata as $member){?>
							<option value="<?php echo $member->ID;?>" <?php selected($member_id,$member->ID);?>><?php echo $member->display_name." - ".$member->member_id; ?> </option>
						<?php }
					 }?>
			</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="notice_content"><?php _e('Start Date','gym_mgt');?><span class="require-field">*</span></label>
			
			<div class="col-sm-8">
			<input id="Start_date" class="datepicker form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $result->start_date;}elseif(isset($_POST['start_date'])){echo $_POST['start_date'];}?>" name="start_date">
				
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="notice_content"><?php _e('End Date','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<input id="end_date" class="datepicker form-control validate[required] text-input" type="text" value="<?php if($edit){ echo $result->expire_date;}elseif(isset($_POST['end_date'])){echo $_POST['end_date'];}?>" name="end_date">
				
			</div>
		</div>
		<!--  
		<div class="form-group">
			<label class="col-sm-2 control-label" for="calories_category"><?php _e('Calories Category','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			
				<select class="form-control" name="calories_id" id="calories_category">
				<option value=""><?php _e('Select Membership Category','gym_mgt');?></option>
				<?php 
				
				if(isset($_REQUEST['calories_category']))
					$category =$_REQUEST['calories_category'];  
				elseif($edit)
					$category =$result->calories_id;
				else 
					$category = "";
				
				$calories_category=gmgt_get_all_category('calories_category');
				if(!empty($calories_category))
				{
					foreach ($calories_category as $retrive_data)
					{
						echo '<option value="'.$retrive_data->ID.'" '.selected($category,$retrive_data->ID).'>'.$retrive_data->post_title.'</option>';
					}
				}
				?>
				</select>
			</div>
			<div class="col-sm-2"><button id="addremove" model="calories_category"><?php _e('Add Or Remove','gym_mgt');?></button></div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="day"><?php _e('Select Day','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<select name="day" class="form-control validate[required] " id="day">
				<option value=""><?php  _e('Select Day ','gym_mgt');?></option>
				<?php $old_day=$result->day;
					foreach(days_array() as $key=>$day)
					{
						echo '<option value='.$key.' '.selected($old_day,$key).'>'.$day.'</option>';
					}?>
				</select>
			</div>
			
		</div>
		-->
		<div class="form-group">
					<label class="col-sm-1 control-label"></label>
			<div class="col-sm-10">
			<div class="col-md-3">
				<?php foreach (days_array() as $key=>$name){?>
				<div class="checkbox">
				  <label><input type="checkbox" value="" name="day[]" value="<?php echo $key;?>" id="<?php echo $key;?>" data-val="day"><?php echo $name; ?> </label>
				</div>
				<?php }?>
			</div>
			<div class="col-md-8 activity_list">
		
				  		<label class="activity_title checkbox">				  		
				  		<strong>
				  			<input type="checkbox" value="" name="avtivity_id[]" value="breakfast" class="nutrition_check" 
				  			id="breakfast"  activity_title = "" data-val="nutrition_time"><?php _e('Break Fast','gym_mgt');?></strong></label>	
				  			<div id="txt_breakfast"></div>
				  			<label class="activity_title checkbox">
				  			<strong>
				  			<input type="checkbox" value="" name="avtivity_id[]" value="lunch" class="nutrition_check" 
				  			id="lunch"  activity_title = "" data-val="nutrition_time"><?php _e('Lunch','gym_mgt');?></strong></label>
				  			<div id="txt_lunch"></div>
				  			<label class="activity_title checkbox"><strong>
				  			<input type="checkbox" value="" name="avtivity_id[]" value="dinner" class="nutrition_check" 
				  			id="dinner"  activity_title = "" data-val="nutrition_time"><?php _e('Dinner','gym_mgt');?></strong></label>	
				  			<div id="txt_dinner"></div>							
						<div class="clear"></div>
					
			</div>
			
			
			
			</div>
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
			<div class="form-group">
				<div class="col-md-8">
					<input type="button" value="<?php _e('Step-1 Add Nutrition');?>" name="save_nutrition" id="add_nutrition" class="btn btn-success"/>
				</div>
			</div>
			</div>
		<div id="display_nutrition_list"></div>
		<div class="clear"></div>
		<!--  <div class="form-group">
			<label class="col-sm-2 control-label" for="breakfast"><?php _e('Breakfast','gym_mgt');?></label>
			<div class="col-sm-8">
			<textarea name="breakfast" class="form-control" id="breakfast"><?php if($edit){ echo $result->breakfast;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="midmorning_snack"><?php _e('Midmorning Snack','gym_mgt');?></label>
			<div class="col-sm-8">
			<textarea name="midmorning_snack" class="form-control" id="midmorning_snack"><?php if($edit){ echo $result->midmorning_snack;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="lunch"><?php _e('Lunch','gym_mgt');?></label>
			<div class="col-sm-8">
			<textarea name="lunch" class="form-control" id="lunch"><?php if($edit){ echo $result->lunch;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="afternoon_snack"><?php _e('Afternoon Snack','gym_mgt');?></label>
			<div class="col-sm-8">
			<textarea name="afternoon_snack" class="form-control" id="afternoon_snack"><?php if($edit){ echo $result->afternoon_snack;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="dinner"><?php _e('Dinner','gym_mgt');?></label>
			<div class="col-sm-8">
			<textarea name="dinner" class="form-control" id="dinner"><?php if($edit){ echo $result->dinner;}?></textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="afterdinner_snack"><?php _e('After Dinner Snack','gym_mgt');?></label>
			<div class="col-sm-8">
			<textarea name="afterdinner_snack" class="form-control" id="afterdinner_snack"><?php if($edit){ echo $result->dinner;}?></textarea>
			</div>
		</div>
		-->
		<div class="col-sm-offset-2 col-sm-8">
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_nutrition" class="btn btn-success"/>
        </div>
		</form>
        </div>
        
     <?php 
     if(isset($nutrition_logdata))
     	foreach($nutrition_logdata as $row){
     	$all_logdata=get_nutritiondata($row->id); //var_dump($workout_logdata);
     
     	$arranged_workout=set_nutrition_array($all_logdata);
     
     	?>
     	 				<div class="workout_<?php echo $row->id;?> workout-block">
     	 				<div class="panel-heading">
						<h3 class="panel-title"><i class="fa fa-calendar"></i> <?php echo "Start From <span class='work_date'>".$row->start_date."</span> To <span class='work_date'>".$row->expire_date; ?> <span class="removenutrition badge badge-delete pull-right" id="<?php echo $row->id;?>">X</span>	</h3>						
					</div>
     	 				
     	 				
     	 				
     	 				<div class="panel panel-white">
     	 					
     	 					<?php
     	 					if(!empty($arranged_workout))
     	 					{
     	 					?>
     	 					<div class="work_out_datalist_header">
     	 					<div class="col-md-4">  
     	 					<strong><?php _e('Day Name','gym_mgt');?></strong>
     	 					</div>
     	 					<div class="col-md-8">
     	 					<span class="col-md-3"><?php _e('Time','gym_mgt');?></span>
     	 					<span class="col-md-3"><?php _e('Description','gym_mgt');?></span>
     	 					
     	 					</div>
     	 					</div>
     	 					<?php 
     	 					foreach($arranged_workout as $key=>$rowdata){?>
     	 				<div class="work_out_datalist">
     	 				<div class="col-md-4 day_name">  
     	 					<?php echo $key;?>
     	 				</div>
     	 				<div class="col-md-8">
     	 						<?php foreach($rowdata as $row){
     	 								echo $row."<br>";
     	 						} ?>
     	 				</div>
     	 				</div>
     	 				<?php } 
     	 					}?>
     	 				
     	 				</div>
     	 			</div>
     	 			<?php }	
	 }
	 ?>
		
	
	</div>
</div>
<?php ?>