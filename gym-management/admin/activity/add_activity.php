<?php ?>
<script type="text/javascript">
$(document).ready(function() {
	$('#acitivity_form').validationEngine();
	
} );
</script>
     <?php 	
	if($active_tab == 'addactivity')
	 {
        	
        	$activity_id=0;
			if(isset($_REQUEST['activity_id']))
				$activity_id=$_REQUEST['activity_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_activity->get_single_activity($activity_id);
					
				}?>
		
       <div class="panel-body">
        <form name="acitivity_form" action="" method="post" class="form-horizontal" id="acitivity_form">
         <?php $action = isset($_REQUEST['action'])?$_REQUEST['action']:'insert';?>
		<input type="hidden" name="action" value="<?php echo $action;?>">
		<input type="hidden" name="activity_id" value="<?php echo $activity_id;?>"  />
		<div class="form-group">
			<label class="col-sm-2 control-label" for="activity_category"><?php _e('Activity Category','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			
				<select class="form-control" name="activity_cat_id" id="activity_category">
				<option value=""><?php _e('Select Activity Category','gym_mgt');?></option>
				<?php 
				
				if(isset($_REQUEST['activity_cat_id']))
					$category =$_REQUEST['activity_cat_id'];  
				elseif($edit)
					$category =$result->activity_cat_id;
				else 
					$category = "";
				
				$activity_category=gmgt_get_all_category('activity_category');
				if(!empty($activity_category))
				{
					foreach ($activity_category as $retrive_data)
					{
						echo '<option value="'.$retrive_data->ID.'" '.selected($category,$retrive_data->ID).'>'.$retrive_data->post_title.'</option>';
					}
				}
				?>
				
				</select>
			</div>
			<div class="col-sm-2"><button id="addremove" model="activity_category"><?php _e('Add Or Remove','gym_mgt');?></button></div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="activity_title"><?php _e('Activity Title','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="activity_title" class="form-control validate[required,custom[onlyLetterSp]] text-input" type="text" value="<?php if($edit){ echo $result->activity_title;}elseif(isset($_POST['activity_title'])) echo $_POST['activity_title'];?>" name="activity_title">
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="staff_name"><?php _e('Assign to Staff Member','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<?php $get_staff = array('role' => 'Staff_member');
					$staffdata=get_users($get_staff);
					
					?>
				<select name="staff_id" class="form-control validate[required] " id="staff_id">
				<option value=""><?php  _e('Select Staff Member ','gym_mgt');?></option>
				<?php 
					
				$staff_data=$result->activity_assigned_to;
					if(!empty($staffdata))
					{
					foreach($staffdata as $staff)
					{
						
						echo '<option value='.$staff->ID.' '.selected($staff_data,$staff->ID).'>'.$staff->display_name.'</option>';
					}
					}
					?>
				</select>
			</div>
			<div class="col-sm-2">
			<a href="?page=gmgt_staff&tab=add_staffmember" class="btn btn-default"> <?php _e('Add Staff Member','gym_mgt');?></a>
			</div>
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php if($edit){ _e('Save','gym_mgt'); }else{ _e('Save','gym_mgt');}?>" name="save_activity" class="btn btn-success"/>
        </div>
		
		
		
        </form>
        </div>
        
     <?php 
	 }
	 ?>