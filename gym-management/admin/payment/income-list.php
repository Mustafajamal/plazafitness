<?php 
	//This is Dashboard at admin side
	//$obj_payment= new Hmgtinvoice();
	$obj_payment= new Gmgtpayment();
	
	if($active_tab == 'incomelist')
	 {
        	$invoice_id=0;
			if(isset($_REQUEST['income_id']))
				$invoice_id=$_REQUEST['income_id'];
			$edit=0;
				if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit'){
					
					$edit=1;
					$result = $obj_payment->hmgt_get_invoice_data($invoice_id);
				}?>
		     <script type="text/javascript">
$(document).ready(function() {
	jQuery('#tblincome').DataTable({
		"responsive": true,
		 "order": [[ 1, "Desc" ]],
		 "aoColumns":[
	                  {"bSortable": true},
	                  {"bSortable": true},
	                  {"bSortable": true}, 
	                  {"bSortable": false}
	               ]
		});
		
	
} );
</script>
     <div class="panel-body">
        	<div class="table-responsive">
        <table id="tblincome" class="display" cellspacing="0" width="100%">
        	 <thead>
            <tr>
				<th> <?php _e( 'Member Name', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Amount', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Date', 'gym_mgt' ) ;?></th>
                <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </thead>
		<tfoot>
            <tr>
				<th> <?php _e( 'Patient Name', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Amount', 'gym_mgt' ) ;?></th>
				<th> <?php _e( 'Date', 'gym_mgt' ) ;?></th>
                <th><?php  _e( 'Action', 'gym_mgt' ) ;?></th>
            </tr>
        </tfoot>
 
        <tbody>
         <?php 
			$paymentdata=$obj_payment->get_all_income_data();
		 	foreach ($paymentdata as $retrieved_data){ 
				$all_entry=json_decode($retrieved_data->entry);
				$total_amount=0;
				foreach($all_entry as $entry){
					$total_amount+=$entry->amount;
				}?>
            <tr>
				<td class="member_name"><?php $user=get_userdata($retrieved_data->supplier_name);
					$memberid=get_user_meta($retrieved_data->supplier_name,'member_id',true);
					$display_label=$user->display_name;
					if($memberid)
						$display_label.=" (".$memberid.")";
					echo $display_label;?></td>
				<td class="income_amount"><?php echo $total_amount;?></td>
                <td class="status"><?php echo $retrieved_data->invoice_date;?></td>
                
               	<td class="action">
				<a  href="#" class="show-invoice-popup btn btn-default" idtest="<?php echo $retrieved_data->invoice_id; ?>" invoice_type="income">
				<i class="fa fa-eye"></i> <?php _e('View Income', 'gym_mgt');?></a>
				<a href="?page=gmgt_payment&tab=addincome&action=edit&income_id=<?php echo $retrieved_data->invoice_id;?>" class="btn btn-info"> <?php _e('Edit', 'gym_mgt' ) ;?></a>
                <a href="?page=gmgt_payment&tab=incomelist&action=delete&income_id=<?php echo $retrieved_data->invoice_id;?>" class="btn btn-danger" 
                onclick="return confirm('<?php _e('Are you sure you want to delete this record?','gym_mgt');?>');">
                <?php _e( 'Delete', 'gym_mgt' ) ;?> </a>
                </td>
            </tr>
            <?php } 
			
		?>
     
        </tbody>
        
        </table>
        </div>
        </div>
	 <?php  }?>