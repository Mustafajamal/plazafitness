<?php 	
if(isset($_POST['save_setting']))
{
	
	$optionval=gmgt_option();
	foreach($optionval as $key=>$val)
	{
		
		if(isset($_POST[$key]))
		{
			$result=update_option( $key, $_POST[$key] );
			
		}
	}
	if(isset($_REQUEST['gym_enable_sandbox']))
			update_option( 'gym_enable_sandbox', 'yes' );
		else 
			update_option( 'gym_enable_sandbox', 'no' );
	if(isset($_REQUEST['gym_enable_memberlist_for_member']))
			update_option( 'gym_enable_memberlist_for_member', 'yes' );
		else 
			update_option( 'gym_enable_memberlist_for_member', 'no' );
	if(isset($result))
	{?>
					<div id="message" class="updated below-h2">
						<p><?php _e('Record updated successfully','gym_mgt');?></p>
					</div>
		<?php 
	}
}

?>
<script type="text/javascript">

$(document).ready(function() {
	$('#setting_form').validationEngine();
} );
</script>
<div class="page-inner" style="min-height:1631px !important">
<div class="page-title">


		<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
	</div>
	<div id="main-wrapper">
	<div class="panel panel-white">
					<div class="panel-body">
<h2>	
		
        	<?php  echo esc_html( __( 'General Settings', 'gym_mgt')); ?>
        </h2>
		<div class="panel-body">
        <form name="setting_form" action="" method="post" class="form-horizontal" id="setting_form">
        <div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_system_name"><?php _e('Gym Name','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_system_name" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_system_name' );?>"  name="gmgt_system_name">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_staring_year"><?php _e('Starting Year','gym_mgt');?></label>
			<div class="col-sm-8">
				<input id="gmgt_staring_year" class="form-control" type="text" value="<?php echo get_option( 'gmgt_staring_year' );?>"  name="gmgt_staring_year">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_gym_address"><?php _e('Gym Address','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_gym_address" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_gym_address' );?>"  name="gmgt_gym_address">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_contact_number"><?php _e('Official Phone Number','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_contact_number" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_contact_number' );?>"  name="gmgt_contact_number">
			</div>
		</div>
		<div class="form-group" class="form-control" id="">
			<label class="col-sm-2 control-label" for="gmgt_contry"><?php _e('Country','gym_mgt');?></label>
			<div class="col-sm-8">
			<!--  <input  class="form-control" type="text" value="<?php echo get_option( 'gmgt_contry' );?>" 
			name="gmgt_contry">-->
			
			<?php 
			$xml=simplexml_load_file(plugins_url( 'countrylist.xml', __FILE__ )) or die("Error: Cannot create object");
			//$url = plugins_url( 'countrylist.xml', __FILE__ );
			//$xml =simplexml_load_string(gmgt_get_remote_file($url));
			?>
			 <select name="gmgt_contry" class="form-control validate[required]" id="smgt_contry">
                        	<option value=""><?php _e('Select Country','gmgt_contry');?></option>
                            <?php
								foreach($xml as $country)
								{  
								?>
								 <option value="<?php echo $country->name;?>" <?php selected(get_option( 'gmgt_contry' ), $country->name);  ?>><?php echo $country->name;?></option>
							<?php }?>
                        </select> 
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_email"><?php _e('Email','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_email" class="form-control validate[required,custom[email]] text-input" type="text" value="<?php echo get_option( 'gmgt_email' );?>"  name="gmgt_email">
			</div>
		</div>
		
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_email"><?php _e('Gym Logo','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			<input type="text" id="gmgt_user_avatar_url" name="gmgt_system_logo" class="validate[required]" value="<?php  echo get_option( 'gmgt_system_logo' ); ?>" />
       				 <input id="upload_user_avatar_button" type="button" class="button" value="<?php _e( 'Upload image', 'gym_mgt' ); ?>" />
       				 <span class="description"><?php _e('Upload image.', 'gym_mgt' ); ?></span>
                     
                     <div id="upload_user_avatar_preview" style="min-height: 100px;">
			<img style="max-width:100%;" src="<?php  echo get_option( 'gmgt_system_logo' ); ?>" />
			
				
			</div>
		</div>
		</div>
			<div class="form-group">
			<label class="col-sm-2 control-label" for="hmgt_cover_image"><?php _e('Profile Cover Image','gym_mgt');?></label>
			<div class="col-sm-8">
			
			<input type="text" id="gmgt_gym_background_image" name="gmgt_gym_background_image" value="<?php  echo get_option( 'gmgt_gym_background_image' ); ?>" />	
       				  <input id="upload_image_button" type="button" class="button upload_user_cover_button" value="<?php _e( 'Upload Cover Image', 'gym_mgt' ); ?>" />
       				 <span class="description"><?php _e('Upload Cover Image', 'gym_mgt' ); ?></span>
                     
                     <div id="upload_gym_cover_preview" style="min-height: 100px;">
			<img style="max-width:100%;" src="<?php  echo get_option( 'gmgt_gym_background_image' ); ?>" />
			
				
			</div>
		</div>
		</div>
		<div class="header">	<hr>
			<h3><?php _e('Measurement Units','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_weight_unit"><?php _e('Weight','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_weight_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_weight_unit' );?>"  name="gmgt_weight_unit">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_height_unit"><?php _e('Height','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_height_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_height_unit' );?>"  name="gmgt_height_unit">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_chest_unit"><?php _e('Chest','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_chest_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_chest_unit' );?>"  name="gmgt_chest_unit">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_waist_unit"><?php _e('Waist','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_waist_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_waist_unit' );?>"  name="gmgt_waist_unit">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_thigh_unit"><?php _e('Thigh','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_thigh_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_thigh_unit' );?>"  name="gmgt_thigh_unit">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_arms_unit"><?php _e('Arms','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_arms_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_arms_unit' );?>"  name="gmgt_arms_unit">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_fat_unit"><?php _e('Fat','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_fat_unit" class="form-control validate[required]" type="text" value="<?php echo get_option( 'gmgt_fat_unit' );?>"  name="gmgt_fat_unit">
			</div>
		</div>
		<div class="header">	<hr>
			<h3><?php _e('Member Privacy Setting','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gym_enable_memberlist_for_member"><?php _e("Member can view other member's details","gym_mgt");?></label>
			<div class="col-sm-8">
				<div class="checkbox">
					<label>
	              		<input type="checkbox" name="gym_enable_memberlist_for_member"  value="yes" <?php echo checked(get_option('gym_enable_memberlist_for_member'),'yes');?>/><?php _e('Enable','gym_mgt');?>
	              </label>
              </div>
			</div>
		</div>
		<div class="header">	<hr>
			<h3><?php _e('Paypal Setting','gym_mgt');?></h3>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gym_enable_sandbox"><?php _e('Enable Sandbox','custom_crm');?></label>
			<div class="col-sm-8">
				<div class="checkbox">
					<label>
	              		<input type="checkbox" name="gym_enable_sandbox"  value="1" <?php echo checked(get_option('gym_enable_sandbox'),'yes');?>/><?php _e('Enable','custom_crm');?>
	              </label>
              </div>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_paypal_email"><?php _e('Paypal Email Id','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
				<input id="gmgt_paypal_email" class="form-control validate[required,custom[email]] text-input" type="text" value="<?php echo get_option( 'gmgt_paypal_email' );?>"  name="gmgt_paypal_email">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="gmgt_currency_code"><?php _e('Select Currency','gym_mgt');?><span class="require-field">*</span></label>
			<div class="col-sm-8">
			
				<select name="gmgt_currency_code" class="form-control validate[required] text-input">
		  <option value=""> <?php _e('Select Currency','gym_mgt');?></option>
		  <option value="AUD" <?php echo selected(get_option( 'gmgt_currency_code' ),'AUD');?>>
		  <?php _e('Australian Dollar','gym_mgt');?></option>
		  <option value="BRL" <?php echo selected(get_option( 'gmgt_currency_code' ),'BRL');?>>
		  <?php _e('Brazilian Real','gym_mgt');?> </option>
		  <option value="CAD" <?php echo selected(get_option( 'gmgt_currency_code' ),'CAD');?>>
		  <?php _e('Canadian Dollar','gym_mgt');?></option>
		  <option value="CZK" <?php echo selected(get_option( 'gmgt_currency_code' ),'CZK');?>>
		  <?php _e('Czech Koruna','gym_mgt');?></option>
		  <option value="DKK" <?php echo selected(get_option( 'gmgt_currency_code' ),'DKK');?>>
		  <?php _e('Danish Krone','gym_mgt');?></option>
		  <option value="EUR" <?php echo selected(get_option( 'gmgt_currency_code' ),'EUR');?>>
		  <?php _e('Euro','gym_mgt');?></option>
		  <option value="HKD" <?php echo selected(get_option( 'gmgt_currency_code' ),'HKD');?>>
		  <?php _e('Hong Kong Dollar','gym_mgt');?></option>
		  <option value="HUF" <?php echo selected(get_option( 'gmgt_currency_code' ),'HUF');?>>
		  <?php _e('Hungarian Forint','gym_mgt');?> </option>
		  <option value="ILS" <?php echo selected(get_option( 'gmgt_currency_code' ),'ILS');?>>
		  <?php _e('Israeli New Sheqel','gym_mgt');?></option>
		  <option value="JPY" <?php echo selected(get_option( 'gmgt_currency_code' ),'JPY');?>>
		  <?php _e('Japanese Yen','gym_mgt');?></option>
		  <option value="MYR" <?php echo selected(get_option( 'gmgt_currency_code' ),'MYR');?>>
		  <?php _e('Malaysian Ringgit','gym_mgt');?></option>
		  <option value="MXN" <?php echo selected(get_option( 'gmgt_currency_code' ),'MXN');?>>
		  <?php _e('Mexican Peso','gym_mgt');?></option>
		  <option value="NOK" <?php echo selected(get_option( 'gmgt_currency_code' ),'NOK');?>>
		  <?php _e('Norwegian Krone','gym_mgt');?></option>
		  <option value="NZD" <?php echo selected(get_option( 'gmgt_currency_code' ),'NZD');?>>
		  <?php _e('New Zealand Dollar','gym_mgt');?></option>
		  <option value="PHP" <?php echo selected(get_option( 'gmgt_currency_code' ),'PHP');?>>
		  <?php _e('Philippine Peso','gym_mgt');?></option>
		  <option value="PLN" <?php echo selected(get_option( 'gmgt_currency_code' ),'PLN');?>>
		  <?php _e('Polish Zloty','gym_mgt');?></option>
		  <option value="GBP" <?php echo selected(get_option( 'gmgt_currency_code' ),'GBP');?>>
		  <?php _e('Pound Sterling','gym_mgt');?></option>
		  <option value="SGD" <?php echo selected(get_option( 'gmgt_currency_code' ),'SGD');?>>
		  <?php _e('Singapore Dollar','gym_mgt');?></option>
		  <option value="SEK" <?php echo selected(get_option( 'gmgt_currency_code' ),'SEK');?>>
		  <?php _e('Swedish Krona','gym_mgt');?></option>
		  <option value="CHF" <?php echo selected(get_option( 'gmgt_currency_code' ),'CHF');?>>
		  <?php _e('Swiss Franc','gym_mgt');?></option>
		  <option value="TWD" <?php echo selected(get_option( 'gmgt_currency_code' ),'TWD');?>>
		  <?php _e('Taiwan New Dollar','gym_mgt');?></option>
		  <option value="THB" <?php echo selected(get_option( 'gmgt_currency_code' ),'THB');?>>
		  <?php _e('Thai Baht','gym_mgt');?></option>
		  <option value="TRY" <?php echo selected(get_option( 'gmgt_currency_code' ),'TRY');?>>
		  <?php _e('Turkish Lira','gym_mgt');?></option>
		  <option value="USD" <?php echo selected(get_option( 'gmgt_currency_code' ),'USD');?>>
		  <?php _e('U.S. Dollar','gym_mgt');?></option>
		</select>
			</div>
		</div>
		
		<div class="col-sm-offset-2 col-sm-8">
        	
        	<input type="submit" value="<?php _e('Save', 'gym_mgt' ); ?>" name="save_setting" class="btn btn-success"/>
        </div>
        
        
        </form>
		</div>
        </div>
        </div>
        </div>
        </div>
 <?php

?> 