<?php $curr_user_id=get_current_user_id();
$obj_gym=new Gym_management($curr_user_id);
$obj_class=new Gmgtclassschedule;
$obj_attend=new Gmgtattendence;
$active_tab = isset($_GET['tab'])?$_GET['tab']:'attendence';
$class_id =0;
?>

<script type="text/javascript">
$(document).ready(function() {
	
	$('#curr_date').datepicker({
		  changeMonth: true,
	        changeYear: true,
	        dateFormat: 'yy-mm-dd',
	        yearRange:'-65:+0',
	        onChangeMonthYear: function(year, month, inst) {
	            $(this).val(month + "/" + year);
	        }
                    
                }); 
		
} );
</script>
<div class="page-inner" style="min-height:1631px !important">
<div class="page-title">
		<h3><img src="<?php echo get_option( 'gmgt_system_logo' ) ?>" class="img-circle head_logo" width="40" height="40" /><?php echo get_option( 'gmgt_system_name' );?></h3>
	</div>
	<?php 
	
	if(isset($_POST['save_attendence']))
	{
		$attend_by=get_current_user_id();
		
		
		$membersdata = get_users(array('meta_key' => 'class_id', 'meta_value' => $_POST['class_id'],'role'=>'member'));
		
				
				$result=$obj_attend->save_attendence($_POST['curr_date'],$_POST['class_id'],$_POST['attendence'],$attend_by,$_POST['status']);
				
			if($result){
			?>
				<div id="message" class="updated below-h2">
						<p><?php _e('Attendance successfully saved!','gym_mgt');?></p>
					</div>
			<?php }
			
		
	}
	//Teacher attendence 
		if(isset($_REQUEST['save_teach_attendence']))
		{
			 
			
			
			$attend_by=get_current_user_id();
			 $result=$obj_attend->save_teacher_attendence($_POST['curr_date'],$_POST['attendence'],$attend_by,$_POST['status'],'staff_member');
			
		
		if($result)
			{?>
				<div id="message" class="updated below-h2">
						<p><?php _e('Attendance successfully saved!','gym_mgt');?></p>
					</div>
	  <?php }
			 
		}
		
		
		if(isset($_REQUEST['action'])&& $_REQUEST['action']=='delete')
			{
				
				$result=$obj_product->delete_product($_REQUEST['product_id']);
				if($result)
				{
					wp_redirect ( admin_url().'admin.php?page=gmgt_product&tab=productlist&message=3');
				}
			}
		if(isset($_REQUEST['message']))
	{
		$message =$_REQUEST['message'];
		if($message == 1)
		{?>
				<div id="message" class="updated below-h2 ">
				<p>
				<?php 
					_e('Record inserted successfully','gym_mgt');
				?></p></div>
				<?php 
			
		}
		elseif($message == 2)
		{?><div id="message" class="updated below-h2 "><p><?php
					_e("Record updated successfully.",'gym_mgt');
					?></p>
					</div>
				<?php 
			
		}
		elseif($message == 3) 
		{?>
		<div id="message" class="updated below-h2"><p>
		<?php 
			_e('Record deleted successfully','gym_mgt');
		?></div></p><?php
				
		}
	}
	?>
	<div id="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-white">
					<div class="panel-body">
	<h2 class="nav-tab-wrapper">
    	<a href="?page=gmgt_attendence&tab=attendence" class="nav-tab <?php echo $active_tab == 'attendence' ? 'nav-tab-active' : ''; ?>">
		<?php echo '<span class="dashicons dashicons-menu"></span> '.__('Attendence', 'gym_mgt'); ?></a>
    	<a href="?page=gmgt_attendence&tab=staff_attendence" class="nav-tab <?php echo $active_tab == 'staff_attendence' ? 'nav-tab-active' : ''; ?>">
		<?php echo '<span class="dashicons dashicons-businessman"></span>'.__('Staff  Attendance', 'gym_mgt'); ?></a>
       
       
    </h2>
     <?php 
	//Report 1 
	
	if($active_tab == 'attendence')
	{ 
	
	?>	
    
	<div class="panel-body"> 
        <form method="post" >  
          <input type="hidden" name="class_id" value="<?php if(isset($class_id))echo $class_id;?>" />
          <div class="form-group col-md-3">
			<label class="control-label" for="curr_date"><?php _e('Date','gym_mgt');?></label>
			
				<input id="curr_date" class="form-control" type="text" value="<?php if(isset($_POST['curr_date'])) echo $_POST['curr_date']; else echo  date("Y-m-d");?>" name="curr_date">
			
		</div>
		<div class="form-group col-md-3">
			<label for="class_id"><?php _e('Select Class','gym_mgt');?></label>			
			<?php $class_id=0; if(isset($_POST['class_id'])){$class_id=$_POST['class_id'];}?>
                 
                    <select name="class_id"  id="class_id"  class="form-control ">
                        <option value=" "><?php _e('Select class Name','gym_mgt');?></option>
                        <?php 
						
						$classdata=$obj_class->get_all_classes();
                          foreach($classdata as $class)
                          {  
                          ?>
                           <option  value="<?php echo $class->class_id;?>" <?php selected($class->class_id,$class_id)?>><?php echo $class->class_name;?></option>
                     <?php }?>
                    </select>
			
		</div>
		 <div class="form-group col-md-3 button-possition">
    	<label for="subject_id">&nbsp;</label>
      	<input type="submit" value="<?php _e('Take/View  Attendance','gym_mgt');?>" name="attendence"  class="btn btn-success"/>
    </div>
       
          </form>
		  </div>
                      <div class="clearfix"> </div>
         <?php 
         if(isset($_REQUEST['attendence']) || isset($_REQUEST['save_attendence']))
         {
			
         	if(isset($_REQUEST['class_id']) && $_REQUEST['class_id'] != " ")
                $class_id =$_REQUEST['class_id'];
         		else 
         			$class_id = 0;
         		if($class_id == 0)
         		{
         		?>
         		<div class="panel-heading">
         	<h4 class="panel-title"><?php _e('Please Select Class','gym_mgt');?></h4>
         </div>
         		<?php }
         		else{
                
               // $membersdata = get_users(array('class_id' => $class_id,'meta_key' =>'membership_status','meta_value'=>'Continue','role'=>'member'));
                  $membersdata = get_users(
                  							array('role'=>'member',
				                  				  'meta_query'=>array(
					                  								array('key' =>'membership_status','value'=>'Continue'),
									                  				array('key'=>'class_id','value'=>$class_id)
								                  				)
							                  )
                  						 );
				
				?>
               <div class="panel-body">  
            <form method="post"  class="form-horizontal">  
          
         
          <input type="hidden" name="class_id" value="<?php echo $class_id;?>" />
          <input type="hidden" name="curr_date" value="<?php if(isset($_POST['curr_date'])) echo $_POST['curr_date']; else echo  date("Y-m-d");?>" />
        
		
         <div class="panel-heading">
         	<h4 class="panel-title"> <?php _e('Class','gym_mgt')?> : <?php echo $class_name=$obj_class->get_class_name($class_id);?> , 
         	<?php _e('Date','gym_mgt')?> : <?php echo $_POST['curr_date'];?></h4>
         </div>
        
          <!--<div class="col-md-12">
        <table class="table">
            <tr><!--  
                <?php if($_REQUEST['curr_date'] == date("Y-m-d")){?> 
                   <th width="100px"><input type="checkbox" name="selectall" id="selectall"/></th>
                  <th width="100px"><?php _e('Status','gym_mgt');?></th>
                  <?php }
                  else {
                  	?>
                  	<th width="100px"><?php _e('Status','gym_mgt');?></th>
                  	<?php 
                  	
                  }  ?>
                  <th><?php _e('Srno','gym_mgt');?></th>
				 
                <th><?php _e('Member Name','gym_mgt');?></th>
                 <th><?php _e('Attendance','gym_mgt');?></th>
            </tr>
            <?php
            $date = $_POST['curr_date'];
            $i = 1;

             foreach($membersdata as $user ) {
            	$date = $_POST['curr_date'];
                   
                    $check_attendance = $obj_attend->check_attendence($user->ID,$class_id,$date);
                    
                    $attendanc_status = "Present";
                    if(!empty($check_attendance))
                    {
                    	$attendanc_status = $check_attendance->status;
                    	
                    }
                   
                echo '<tr>';
              
                echo '<td>'.$i.'</td>';
				
                echo '<td><span>' .$user->display_name.'</span></td>';
                ?>		
                <td><label class="radio-inline"><input type="radio" name = "attendanace_<?php echo $user->ID?>" value ="Present" <?php checked( $attendanc_status, 'Present' );?>>
                <?php _e('Present','gym_mgt');?></label>
				<label class="radio-inline"> <input type="radio" name = "attendanace_<?php echo $user->ID?>" value ="Absent" <?php checked( $attendanc_status, 'Absent' );?>>
				<?php _e('Absent','gym_mgt');?></label></td><?php 
                
                echo '</tr>';
                $i++;}?>
                   
					</table>

		
					</div> -->
					<?php if($_REQUEST['curr_date'] == date("Y-m-d")){?> 
          <div class="form-group">
          <label class="radio-inline">
          <input type="radio" name="status" value="Present" checked="checked"/> <?php _e('Present','gym_mgt');?>
          </label>
          <label class="radio-inline">
          <input type="radio" name="status" value="Absent" /> <?php _e('Absent','gym_mgt');?><br />
          </label>
          </div>
          
          <?php }?>
          <div class="col-md-12">
        <table class="table">
            <tr>
                <?php if($_REQUEST['curr_date'] == date("Y-m-d")){?> 
                  <th width="46px"><input type="checkbox" name="selectall" id="selectall"/></th>
                  <?php }
                  else {
                  	?>
                  	<th width="70px"><?php _e('Status','gym_mgt');?></th>
                  	<?php 
                  	
                  }  ?>
                <th width="250px"><?php _e('Member Name','gym_mgt');?></th>
				<?php if($_REQUEST['curr_date'] == date("Y-m-d")){?>
                <th><?php _e('Status','gym_mgt');?></th>
				<?php } ?>
            </tr>
            <?php
            $date = $_POST['curr_date'];
             foreach ( $membersdata as $user ) {
            	$date = $_POST['curr_date'];
                    $check_result=$obj_attend->check_attendence($user->ID,$class_id,$date);
					
					
                echo '<tr>';
                 if($_REQUEST['curr_date'] == date("Y-m-d")){?> 
                <td class="checkbox_field"><span><input type="checkbox" class="checkbox1" name="attendence[]" value="<?php echo $user->ID; ?>" <?php if($check_result=='true'){ echo "checked=\'checked\'"; } ?> /></span></td>
                <?php }
                else 
                {
                	?>
                	<td><?php if($check_result=='true') _e('Present','gym_mgt'); else _e('Absent','gym_mgt');?></td>
                	<?php 
                }
             
                echo '<td><span>' .$user->first_name.' '.$user->last_name.' ('.$user->member_id.')</span></td>';
                if($_REQUEST['curr_date'] == date("Y-m-d")){
					if(!empty($check_result)){ 
						 echo '<td><span>' .$check_result->status.'</span></td>';
					}
					else 
						echo '<td>&nbsp;</td>';
                }
				
                echo '</tr>';
					
                }?>
                   
					</table>
					</div>
					
					
					
					<div class="col-sm-12"> 
					<?php if($_REQUEST['curr_date'] == date("Y-m-d")){?>       	
        	<input type="submit" value="<?php _e('Save Attendance','gym_mgt');?>" name="save_attendence" class="btn btn-success" />
        	<?php }?>
        </div>
       
        </form>	</div>
        <?php }
         }
        ?>
        
	<?php } 
	if($active_tab == 'staff_attendence')
	{ 
		require_once GMS_PLUGIN_DIR. '/admin/attendence/staff-attendence.php';
	}?>
</div>
			
	</div>
	</div>
</div>
</div>