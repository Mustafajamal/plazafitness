<?php 
//$user = new WP_User($user_id);
	  
class Gmgtclassschedule
{	

	
	public function gmgt_add_class($data)
	{
		
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
		//-------usersmeta table data--------------
		$classdata['class_name']=$data['class_name'];
		$classdata['staff_id']=$data['staff_id'];
		$classdata['asst_staff_id']=$data['asst_staff_id'];
		$classdata['day']=json_encode($data['day']);
		$classdata['start_time']=$data['start_time'].':'.$data['start_min'].':'.$data['start_ampm'];
		$classdata['end_time']=$data['end_time'].':'.$data['end_min'].':'.$data['end_ampm'];
		$classdata['staff_id']=$data['staff_id'];
		$classdata['class_creat_date']=date("Y-m-d");
		$classdata['class_created_id']=get_current_user_id();
		
	
		if($data['action']=='edit')
		{
			$classid['class_id']=$data['class_id'];
			$result=$wpdb->update( $table_class, $classdata ,$classid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_class, $classdata );
			return $result;
		}
	
	}
	public function get_all_classes()
	{
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
	
		$result = $wpdb->get_results("SELECT * FROM $table_class");
		return $result;
	
	}
	public function get_single_class($id)
	{
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
		$result = $wpdb->get_row("SELECT * FROM $table_class where class_id=".$id);
		return $result;
	}
	public function get_class_name($id)
	{
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
		$result = $wpdb->get_row("SELECT class_name FROM $table_class where class_id=".$id);
		return $result->class_name;
	}
	public function delete_class($id)
	{
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
		$result = $wpdb->query("DELETE FROM $table_class where class_id= ".$id);
		return $result;
	}
	public function get_schedule_byday($day)
	{
		global $wpdb;
		$table_class = $wpdb->prefix. 'gmgt_class_schedule';
		$resultdata = $wpdb->get_results("SELECT * FROM $table_class ORDER BY start_time  ASC");
		//var_dump($result);
		$day_array[]=array();
		foreach($resultdata as  $result){
		$class_days=json_decode($result->day);
			if(in_array($day,$class_days))
			{
				$day_array[]=array('dayname'=>$day,'start_time'=>$result->start_time,'end_time'=>$result->end_time,'class_id'=>$result->class_id,'staff_id'=>$result->staff_id,'asst_staff_id'=>$result->asst_staff_id);
			}
		
		}
		//var_dump($day_array);
		
		return $day_array;
	}
	
	
}
?>