<?php 
//$user = new WP_User($user_id);
	  
class Gmgtworkouttype
{	

	
	public function gmgt_add_workouttype($data)
	{
		
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_assign_workout';
		
		$workoutdata['user_id']=$data['member_id'];
		
		$workoutdata['level_id']=$data['level_id'];
		$workoutdata['description']=$data['description'];
		
		$workoutdata['start_date']=$data['start_date'];
		$workoutdata['end_date']=$data['last_date'];
		$workoutdata['created_date']=date("Y-m-d");
		$workoutdata['created_by']=get_current_user_id();
		
		$new_array = array();
		$i = 0;
		
		
       //print_r($phpobj);
		//echo $phpobj;
		$phpobj = array();
		foreach($data['activity_list'] as $val)
		{
			$data_value = json_decode($val);
			$phpobj[] = json_decode(stripslashes($val),true);
			
		}
		//var_dump($phpobj);
		
		$j=0;
		$final_array = array();
		$resultarray =array();
		foreach($phpobj as $key => $val)
		{
			//var_dump($val);
			$day = array();
			$activity = array();
			foreach($val as $key=>$key_val)
			{
				
				//$activity =array();
				if($key == "days")
				foreach($key_val as $val1)
				{
					$day['day'][] =$val1['day_name'] ;
				}
				if($key == "activity")
					foreach($key_val as $val2)
					{
						//var_dump($val2);
						echo $val2['activity']['activity'];
						$activity['activity'][] =array('activity'=>$val2['activity']['activity'],
													'reps'=>$val2['activity']['reps'],
													'sets'=>$val2['activity']['sets'],
													'kg'=>$val2['activity']['kg'],
													'time'=>$val2['activity']['time'],
						) ;
					}
				
			}
			$resultarray[] = array_merge($day, $activity);
			
		
			
		}
		
		if($data['action']=='edit')
		{
			$workoutid['id']=$data['assign_workout_id'];	
			$result=$wpdb->update( $table_workout, $workoutdata ,$workoutid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_workout, $workoutdata );
			$assign_workout_id = $wpdb->insert_id;
			$this->assign_workout_detail($assign_workout_id,$resultarray);
			return $result;
		}
	
	}
	public function assign_workout_detail($workout_id,$work_outdata)
	{
		if(!empty($work_outdata))
		{
			global $wpdb;
			$table_workout = $wpdb->prefix. 'gmgt_workout_data';
			$workout_data = array();
			foreach($work_outdata as  $value)
			{
				//var_dump($value);
				foreach($value['day'] as $day)
				{
					echo "day".$day;
					foreach($value['activity']  as $actname)
					{
						//var_dump($actname);
						$workout_data['day_name'] = $day;
						$workout_data['workout_name'] = $actname['activity'];
						$workout_data['sets'] = $actname['sets'];
						$workout_data['reps'] = $actname['reps'];
						$workout_data['kg'] = $actname['kg'];
						$workout_data['time'] = $actname['time'];
						$workout_data['workout_id'] = $workout_id;
						$workout_data['created_date'] = date("Y-m-d");
						$workout_data['create_by'] = get_current_user_id();
						$result=$wpdb->insert( $table_workout, $workout_data );
					}
				}
				/* foreach($value as $key => $val1)
				 {
				 var_dump($val1);
				 echo $key;
				 } */
			}
			
			
			
			
			/* foreach($work_outdata as $day => $value)
			{
				
				foreach($value as  $val)
				{
					$workout_data['day_name'] = $day;
					$workout_data['workout_name'] = $val['activity'];
					$workout_data['sets'] = $val['sets'];
					$workout_data['reps'] = $val['reps'];
					$workout_data['workout_id'] = $workout_id;
					$workout_data['created_date'] = date("Y-m-d");
					$workout_data['create_by'] = get_current_user_id();
					$result=$wpdb->insert( $table_workout, $workout_data );
				}
			} */
		}
	}
	public function get_all_assignworkout()
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_assign_workout';
	
		$result = $wpdb->get_results("SELECT * FROM $table_workout");
		return $result;
	
	}
	public function get_all_workouttype()
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_workouts';
	
		$result = $wpdb->get_results("SELECT * FROM $table_workout");
		return $result;
	
	}
	public function get_own_assigned_workout($role,$id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_workouts';
		if($role=='member')
			$result = $wpdb->get_results("SELECT * FROM $table_workout where member_id=".$id);
		else
			$result = $wpdb->get_results("SELECT * FROM $table_workout where created_by=".$id);
		return $result;
	
	}
	public function get_single_workouttype($id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_workouts';
		$result = $wpdb->get_row("SELECT * FROM $table_workout where id=".$id);
		return $result;
	}
	public function get_assigned_workout($id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_workouts';
		$result = $wpdb->get_results("SELECT * FROM $table_workout where member_id=".$id);
		return $result;
	}
	public function delete_workouttype($id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix. 'gmgt_workouts';
		$result = $wpdb->query("DELETE FROM $table_workout where id= ".$id);
		return $result;
	}
	public function get_single_workoutdata($id)
	{
		global $wpdb;
		$table_workout = $wpdb->prefix.'gmgt_workout_data';
		$result = $wpdb->get_row("SELECT *FROM $table_workout where id=".$id);
		
		return $result;
	}
	
	
}
?>