<?php 
//$user = new WP_User($user_id);
	  
class Gmgtmembership
{	

	
	public function gmgt_add_membership($data,$member_image_url)
	{
		
		global $wpdb;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
		//-------usersmeta table data--------------
		$membershipdata['membership_label']=$data['membership_name'];
		$membershipdata['membership_cat_id']=$data['membership_category'];
		$membershipdata['membership_length_id']=$data['membership_period'];
		$membershipdata['membership_class_limit']=$data['member_limit'];
		$membershipdata['install_plan_id']=$data['installment_plan'];
		$membershipdata['membership_amount']=$data['membership_amount'];
		$membershipdata['installment_amount']=$data['installment_amount'];
		$membershipdata['signup_fee']=$data['signup_fee'];
		$membershipdata['gmgt_membershipimage']=$member_image_url;
		$membershipdata['created_date']=date("Y-m-d");
		$membershipdata['created_by_id']=get_current_user_id();
		
	
		if($data['action']=='edit')
		{
			$membershipid['membership_id']=$data['membership_id'];
			$result=$wpdb->update( $table_membership, $membershipdata ,$membershipid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_membership, $membershipdata );
			return $result;
		}
	
	}
	public function get_all_membership()
	{
		global $wpdb;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
	
		$result = $wpdb->get_results("SELECT * FROM $table_membership");
		return $result;
	
	}
	public function get_single_membership($id)
	{
		global $wpdb;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
		$result = $wpdb->get_row("SELECT * FROM $table_membership where membership_id= ".$id);
		return $result;
	}
	public function delete_membership($id)
	{
		global $wpdb;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
		$result = $wpdb->query("DELETE FROM $table_membership where membership_id= ".$id);
		return $result;
	}
	public function update_membershipimage($id,$imagepath)
	{
		global $wpdb;
		$table_membership = $wpdb->prefix. 'gmgt_membershiptype';
		$image['gmgt_membershipimage']=$imagepath;
		$membershipid['membership_id']=$id;
		return $result=$wpdb->update( $table_membership, $image, $membershipid);
	}
	
	
}
?>