<?php 
//$user = new WP_User($user_id);
	  
class Gmgtreservation
{	

	
	public function gmgt_add_reservation($data)
	{
		
		global $wpdb;
		$table_reservation = $wpdb->prefix. 'gmgt_reservation';
		//-------usersmeta table data--------------
		$reservationdata['event_name']=$data['event_name'];
		$reservationdata['event_date']=$data['event_date'];
		$reservationdata['start_time']=$data['start_time'].':'.$data['start_min'].':'.$data['start_ampm'];
		$reservationdata['end_time']=$data['end_time'].':'.$data['end_min'].':'.$data['end_ampm'];
		$reservationdata['place_id']=$data['event_place'];
		$reservationdata['created_date']=date("Y-m-d");
		$reservationdata['created_by']=get_current_user_id();
		
		$reserv_datedata=$this->get_all_reservation();
		$same_date="";
		foreach($reserv_datedata as $retrieved_data)
		{
			if($retrieved_data->event_date == $data['event_date'] && $retrieved_data->id!=$data['reservation_id'])
			{
				$same_date=$retrieved_data->event_date;
				
			}
		}
		
		if($data['action']=='edit')
		{
			$reservationid['id']=$data['reservation_id'];
			if($same_date=="")
				$result=$wpdb->update( $table_reservation, $reservationdata ,$reservationid);
			else
				$result=array('id'=>$data['reservation_id'],'msg'=>'reserved');
			return $result;
		}
		else
		{
			
			if($same_date=="")
				$result=$wpdb->insert( $table_reservation, $reservationdata );
			else
				$result="reserved";
			return $result;
		}
	
	}
	public function get_all_reservation()
	{
		global $wpdb;
		$table_reservation = $wpdb->prefix. 'gmgt_reservation';
	
		$result = $wpdb->get_results("SELECT * FROM $table_reservation");
		return $result;
	
	}
	public function get_single_reservation($id)
	{
		global $wpdb;
		$table_reservation = $wpdb->prefix. 'gmgt_reservation';
		$result = $wpdb->get_row("SELECT * FROM $table_reservation where id=".$id);
		return $result;
	}
	public function delete_reservation($id)
	{
		global $wpdb;
		$table_reservation = $wpdb->prefix. 'gmgt_reservation';
		$result = $wpdb->query("DELETE FROM $table_reservation where id= ".$id);
		return $result;
	}
	
	
}
?>