<?php 
//$user = new WP_User($user_id);
	  
class Gmgtpayment
{	

	
	public function gmgt_add_payment($data)
	{
		
		global $wpdb;
		$table_payment=$wpdb->prefix.'gmgt_payment';
		$paymentdata['title']=$data['payment_title'];
		$paymentdata['member_id']=$data['member_id'];
		$paymentdata['due_date']=$data['due_date'];
		//$paymentdata['unit_price']=$data['amount'];
		$paymentdata['total_amount']=$data['total_amount'];
		$paymentdata['discount']=$data['discount'];
		$paymentdata['payment_status']=$data['payment_status'];
		$paymentdata['description']=$data['description'];
		$paymentdata['payment_date']=date("Y-m-d");
		$paymentdata['receiver_id']=get_current_user_id();
		
		if($data['action']=='edit')
		{
			$paymentid['payment_id']=$data['payment_id'];
			$result=$wpdb->update( $table_payment, $paymentdata ,$paymentid);
			return $result;
		}
		else
		{
			
			$result=$wpdb->insert( $table_payment,$paymentdata);
			return $result;
		}
	
	}
	public function get_all_payment()
	{
		global $wpdb;
		$table_payment = $wpdb->prefix. 'gmgt_payment';
	
		$result = $wpdb->get_results("SELECT * FROM $table_payment");
		return $result;
	
	}
	public function get_single_payment($id)
	{
		global $wpdb;
		$table_payment = $wpdb->prefix. 'gmgt_payment';
		$result = $wpdb->get_row("SELECT * FROM $table_payment where payment_id=".$id);
		return $result;
	}
	public function get_own_payment($id)
	{
		global $wpdb;
		$table_payment = $wpdb->prefix. 'gmgt_payment';
		$result = $wpdb->get_results("SELECT * FROM $table_payment where member_id=".$id);
		return $result;
	}
	public function delete_payment($id)
	{
		global $wpdb;
		$table_payment = $wpdb->prefix. 'gmgt_payment';
		$result = $wpdb->query("DELETE FROM $table_payment where payment_id= ".$id);
		return $result;
	}
	//--------Income----------------
	public function get_entry_records($data)
	{
			$all_income_entry=$data['income_entry'];
			 $all_income_amount=$data['income_amount'];
			
			$entry_data=array();
			$i=0;
			foreach($all_income_entry as $one_entry)
			{
				$entry_data[]= array('entry'=>$one_entry,
							'amount'=>$all_income_amount[$i]);
					$i++;
			}
			return json_encode($entry_data);
	}
	public function add_income($data)
	{
		
		$entry_value=$this->get_entry_records($data);
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$incomedata['invoice_type']=$data['invoice_type'];
		$incomedata['invoice_label']=$data['invoice_label'];
		$incomedata['supplier_name']=$data['supplier_name'];
		
		$incomedata['invoice_date']=$data['invoice_date'];
		
		$incomedata['payment_status']=$data['payment_status'];
		$incomedata['entry']=$entry_value;
		$incomedata['receiver_id']=get_current_user_id();
		
		if($data['action']=='edit')
		{
			$income_dataid['invoice_id']=$data['income_id'];
			$result=$wpdb->update( $table_income, $incomedata ,$income_dataid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_income,$incomedata);
			return $result;
		}
	}
	public function get_all_income_data()
	{
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		
		$result = $wpdb->get_results("SELECT * FROM $table_income where invoice_type='income'");
		return $result;
		
	}
	public function delete_income($income_id)
	{
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$result = $wpdb->query("DELETE FROM $table_income where invoice_id= ".$income_id);
		return $result;
	}
	public function gmgt_get_income_data($income_id)
	{
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
	
		$result = $wpdb->get_row("SELECT * FROM $table_income where invoice_id= ".$income_id);
		return $result;
	}
	//-----------Expense-----------------
	public function add_expense($data)
	{
		
		$entry_value=$this->get_entry_records($data);
		
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		$incomedata['invoice_type']=$data['invoice_type'];
		$incomedata['supplier_name']=$data['supplier_name'];
		
		$incomedata['invoice_date']=$data['invoice_date'];
		
		$incomedata['payment_status']=$data['payment_status'];
		$incomedata['entry']=$entry_value;
		$incomedata['receiver_id']=get_current_user_id();
		
		if($data['action']=='edit')
		{
			$expense_dataid['invoice_id']=$data['expense_id'];
			$result=$wpdb->update( $table_income, $incomedata ,$expense_dataid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_income,$incomedata);
			return $result;
		}
	}
	public function get_all_expense_data()
	{
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
		
		$result = $wpdb->get_results("SELECT * FROM $table_income where invoice_type='expense'");
		return $result;
		
	}
	public function get_oneparty_income_data($party_id)
	{
		global $wpdb;
		$table_income=$wpdb->prefix.'gmgt_income_expense';
	
		$result = $wpdb->get_results("SELECT * FROM $table_income where supplier_name= '".$party_id."' order by invoice_date desc");
		
		return $result;
	}
	
}
?>