<?php 
//$user = new WP_User($user_id);
	  
class Gmgtactivity
{	

	
	public function gmgt_add_activity($data)
	{
		
		global $wpdb;
		$table_activity = $wpdb->prefix. 'gmgt_activity';
		
		$activitydata['activity_cat_id']=$data['activity_cat_id'];
		$activitydata['activity_title']=$data['activity_title'];
		$activitydata['activity_assigned_to']=$data['staff_id'];
		$activitydata['activity_added_date']=date("Y-m-d");
		$activitydata['activity_added_by']=get_current_user_id();
		
		
		if($data['action']=='edit')
		{
			$activityid['activity_id']=$data['activity_id'];
			$result=$wpdb->update( $table_activity, $activitydata ,$activityid);
			return $result;
		}
		else
		{
			$result=$wpdb->insert( $table_activity, $activitydata );
			return $result;
		}
	
	}
	public function get_all_activity()
	{
		global $wpdb;
		$table_activity = $wpdb->prefix. 'gmgt_activity';
	
		$result = $wpdb->get_results("SELECT * FROM $table_activity");
		return $result;
	
	}
	public function get_single_activity($id)
	{
		global $wpdb;
		$table_activity = $wpdb->prefix. 'gmgt_activity';
		$result = $wpdb->get_row("SELECT * FROM $table_activity where activity_id=".$id);
		return $result;
	}
	public function delete_activity($id)
	{
		global $wpdb;
		$table_activity = $wpdb->prefix. 'gmgt_activity';
		$result = $wpdb->query("DELETE FROM $table_activity where activity_id= ".$id);
		return $result;
	}
	
	
}
?>